#include "defs.h"

void RankBoost(X, Y, m, d, RBparams, theta, j_etoile, theta_etoile, MaxThetas, MinThetas, alpha, nu, param_filename)
SPARSEVECT *X;        // Matrice des représentations creuses des exemples
long int *Y;          // Vecteur contenant les jugements de pertinence des données
long int m;           // Nombre d'exemples dans la base d'entraînement
long int d;           // Dimension de l'espace de représentation
LR_PARAM RBparams;    // /*@Param\`etres d'entr\'ee@*/
double *theta;        // Vecteur des seuils /*@ $\theta_1\leq \ldots \leq \theta_p$ @*/ 
long int *j_etoile;   // /*@Vecteur contenant les caract\'eristiques choisies @*/
double *theta_etoile; // Vecteur contenant les seuils choisis   
double *MaxThetas;    // Bornes sup\'erieures sur les seuils pour toutes les caractéristiques /*@$\forall j, \theta_p^j$@*/
double *MinThetas;    // Bornes inf\'erieures sur les seuils pour toutes les caractéristiques /*@$\forall j, \theta_1^j$@*/
double *alpha;        // poids de la combinaison  
double *nu;           // Distribution sur les exemples  
char *param_filename; // Fichier contenant les param\`etres   
{
  long int a, t, i, per=0, nper=0;
  double   Rt=0.1, Zt1, Zt_1;
  FILE     *fd;

  if((fd=fopen(param_filename,"w"))==NULL){
    printf("Impossible de créer le fichier : %s\n",param_filename);
    exit(0);
  }
  fprintf(fd,"# alpha j* theta*\n");

  for(i=1; i<=m; i++){
    if(Y[i]==1)
       per++;
    else if(Y[i]==-1)
       nper++;
  }

  /*@ Initialisation de la distribution des exemples  $\rhd~
\forall i\in \{1,...,m\}, \nu_1(i)=\left \lbrace \begin{array}{l} \frac{1}{n_+} \mbox{~si~}y_i=1 \\ \frac{1}{n_-} \mbox{~si~}y_i=-1 \end{array} \right. 
$ (Eq. \ref{eq:DistInit}) @*/
  for(i=1; i<=m; i++){
    if(Y[i]==1)
       nu[i]=1.0/(double )per;
    else if(Y[i]==-1)
       nu[i]=1.0/(double )nper;
  }
  printf("Apprentissage ...\n");
  for(t=1; t<=RBparams.T; t++){
    // Sélectionner la règle d'ordonnancement à partir de la distribution courante
    WeakLearner(X, Y, m, d, nu, RBparams.p, &Rt, &theta_etoile[t], &j_etoile[t], theta, MaxThetas, MinThetas);

    // /*@ $\rhd ~\alpha_t=\frac{1}{2}\ln\left(\frac{1+r_t}{1-r_t}\right)$ (Eq. \ref{eq:alpha} @*/    
    alpha[t]=0.5*log((1.0+Rt)/(1.0-Rt));
   
    // Mettre à jour les distributions /*@$\rhd~$ \'equations \eqref{eq:MiseAJourDist}, \eqref{eq:DecDt} et \eqref{eq:ZtZZ} @*/
    Zt1=Zt_1=0.0;
    for(i=1; i<=m; i++){
       /*@ pour l'exemple courant $\obs_i$ si la $j^*$-\`eme caract\'eristique  est pr\'esente et que sa valeur $\varphi_{j^*}(\obs_i)$ est sup\'erieure \`a $\theta^*$, modifier sa distribution @*/
      for(a=1; a<=X[i].N && X[i].Att[a].ind<j_etoile[t]; a++);
      if(X[i].Att[a].ind==j_etoile[t] && X[i].Att[j_etoile[t]].val>=theta_etoile[t])
          nu[i]*=exp(-1.0*Y[i]*alpha[t]);
      
      if(Y[i]==1)
        Zt1+=nu[i];
      else if(Y[i]==-1) 
        Zt_1+=nu[i];
    }
    for(i=1; i<=m; i++){
      if(Y[i]==1)
        nu[i]/=Zt1;
      else if(Y[i]==-1) 
        nu[i]/=Zt_1;
    }

    fprintf(fd,"%.16lf %ld %.16lf\n",alpha[t],j_etoile[t],theta_etoile[t]);
    if(RBparams.display)
		printf("%4ld --> alpha= %3.16lf j*=%4ld theta*=%1.8lf\n",t, alpha[t],j_etoile[t],theta_etoile[t]);
  }
  
  fclose(fd);
}


void WeakLearner(X, Y, m, d, nu, p, r_et, theta_et, j_et, theta, MaxThetas,MinThetas)
SPARSEVECT *X;     // /*@Matrice des repr\'esentations creuses des exemples@*/
long int *Y;       // /*@Vecteur contenant les jugements de pertinence des donn\'ees@*/
long int m;        // Nombre d'exemples dans la base d'entrainement
long int d;        // /*@Dimension de l'espace de repr\'esentation@*/
double *nu;        // Distribution sur les exemples  
long int p;        // /*@Nombre de seuils sur les caract\'eristiques@*/
double *r_et;      // /*@La fonction $r$ (Eq. \ref{eq:ch6:r}) qu'on cherche \`a maximiser@*/
double *theta_et;  // Le seuil /*@$\theta^*$@*/ choisi
long int *j_et;    // /*@La caract\'eristique $ j^* $ choisie @*/
double *theta;     // Le vecteur qui va contenir tous les seuils /*@ $\theta_1\leq \ldots \leq \theta_p$ @*/
double *MaxThetas; // /*@Bornes sup\'erieures sur les seuils pour toutes les caract\'eristiques $\forall j, \theta_p^j$@*/
double *MinThetas; // /*@Bornes inf\'erieures sur les seuils pour toutes les caract\'eristiques $\forall j, \theta_1^j$@*/
{
  long int i, j, l, a;
  double L,  pas, x, *sommePI;
  
  sommePI=    (double *)Allocation((p+1)*sizeof(double));
 
   
  *r_et=0.0;
  for(j=1; j<=d; j++){
    pas=(MaxThetas[j]-MinThetas[j])/(double )p;

    for(l=1; l<=p; l++){
      sommePI[l]=0.0;
      theta[l]=MinThetas[j]+((double )(l-1))*pas;
    }
    // Calcul de /*@ $\text{SommePI}_l\leftarrow \sum_{\obs_i:\theta_{\ell-1}\geq \varphi_j(\obs_i)>\theta_\ell}\pi(\obs_i)$@*/
    for(i=1; i<=m; i++){
        for(a=1; a<=X[i].N && X[i].Att[a].ind<j; a++);
        if(X[i].Att[a].ind==j){
          x=X[i].Att[a].val;
          l=1;
	      while(l<p && x>=theta[l])
	        l++;
	      l--;
          //printf("%ld\n",i);
          if(l<p)
            sommePI[l]+=(Y[i]*nu[i]);
	      else
	        sommePI[p]+=(Y[i]*nu[i]);
       }
     }
     // On suppose que les valeurs de toutes les caractéristiques sont connues /*@ $R=0$@*/
     L=0.0;
     for(l=p;l>=1; l--){
       L+= sommePI[l]; 
       if(fabs(L)>fabs(*r_et)){
         *r_et=L;
 	     *theta_et=theta[l];
	     *j_et=j;
       }
     }
   }
   free((char *) sommePI);
}
