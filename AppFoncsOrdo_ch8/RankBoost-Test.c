//  *******************************************************************************  //       
//                                                                                   //
//                                 SSRankBoost-Test.c                                //
//                                                                                   //
//   Semi-supervised RankBoost: test module                                          //
//                                                                                   //
//   Author: Massih R. Amini                                                         //
//   Date: 23/03/2016                                                                //
//                                                                                   //
//   Copyright (c) 2016  Massih R. Amini  - All rights reserved                      //
//                                                                                   //
//   This software is available for non-commercial use only. It must not be modified //
//   and distributed without prior permission of the author. The author is not       //
//   responsible for implications from the use of this software.                     //
//                                                                                   //
//  *******************************************************************************  //
#include "defs.h"


int main(int argc, char **argv)
{
  long int   i, *i_etoile, m, d,*Y, K,  MaxTaille;
  double     *theta,*alpha, *Sortie;
  SPARSEVECT *X;
  LR_PARAM   RBparams;

  if(argc!=3)
  {
    printf("Rankboost-test datafile paramsfils\n");
    exit(0);
  }

  FileSparseVectors(argv[1],&m,&K,&d,&MaxTaille);
  printf("La base de test contient %ld exemples en dimension %ld\n",m,d);

  X=(SPARSEVECT *)Allocation((m+1)*sizeof(SPARSEVECT ));
  for(i=1; i<=m; i++)
     X[i].Att=(FEATURE *)Allocation((MaxTaille+1)*sizeof(FEATURE ));

  Y=(long int *)Allocation((m+1)*sizeof(long int ));
  
  ChrgSparseMatrixRB_Tst(argv[1], X, Y, m);
  Sortie=    (double *)malloc((m+1)*sizeof(double ));

  ParamFileSize(argv[2],&RBparams.T);
  theta=    (double *)malloc((RBparams.T+1)*sizeof(double ));
  alpha=    (double *)malloc((RBparams.T+1)*sizeof(double ));
  i_etoile= (long int *)malloc((RBparams.T+1)*sizeof(long int ));
  ReadParams(argv[2],RBparams.T,alpha,i_etoile,theta);


  H(alpha,i_etoile,theta,RBparams.T,Sortie,m,X);
  Evaluate(Sortie,Y,m);
  return 1;
}

void ParamFileSize(char *paramsfile, long int *T)
{
  FILE *fd;
  long int n=0;
  char c;
  
  if((fd=fopen(paramsfile,"r"))==NULL){
    printf("Enable to open: %s\n",paramsfile);
    exit(0);
  }
  while((c=fgetc(fd))!=EOF)
    if(c=='\n')
      n++;
  
  *T=n-1;
  fclose(fd);
}

void ReadParams(char *paramsfile, long int T, double *alpha, long int *i_etoile, double *theta)
{
  FILE *fd;
  char c;
  long int i;
   
  if((fd=fopen(paramsfile,"r"))==NULL){
    printf("Enable to open: %s\n",paramsfile);
    exit(0);
  }
  while((c=fgetc(fd))!='\n');
 
  
  for(i=1; i<=T; i++)
   fscanf(fd,"%lf %ld %lf",&alpha[i],&i_etoile[i],&theta[i]);
   
    
  fclose(fd);
}

void H(double *alpha, long int *j_etoile, double *theta, long int T,  double *sortie, long int m, SPARSEVECT *X)
{
  long int t, i, a;
  
  for(i=1; i<=m; i++){
     sortie[i]=0.0;
     for(t=1; t<=T; t++){
        for(a=1; a<=X[i].N && X[i].Att[a].ind<j_etoile[t]; a++);
        if(X[i].Att[a].ind==j_etoile[t] && X[i].Att[j_etoile[t]].val>=theta[t])
          sortie[i]+=alpha[t];
     }
  }
}

void Evaluate(double *sortie, long int *Desire, long int sT) 
{
  long int *indx, i, positif=0, negatif=0;
  double AvP=0.0,AUC=0.0,REC=0.0;

  indx=(long int *)malloc((sT+1)*sizeof(long int ));
  indexx(sT, sortie, indx);
  for(i=1; i<=sT; i++){
    if(Desire[i]==1)
      positif++;
    else
      negatif++;
  }

  for(i=1; i<=sT; i++){
    if(Desire[indx[sT-i+1]]==1){
      REC+=1.0;
      AvP+=REC/((double) i);
      AUC+=((double) i)-REC;
    }

  }
  printf("AUC=%lf AvP=%lf\n",1.0-AUC/(double )(positif*negatif),AvP/REC);
  free(indx);
}


void indexx(long int n, double *VL, long int *indx)
{
  long int indxt, itemp, ir, i,j,l, k;
  long int jstack=0, *istack;
  double a;

  istack=(long int *)malloc((NSTACK+1)*sizeof(long int ));
		
  l=1;
  jstack=0;
  ir=n;
  for(j=1; j<=n; j++) indx[j]=j;
    for(;;){
      if(ir-l < M){
	for(j=l+1; j<=ir; j++){
	   indxt=indx[j];
	   a=VL[indxt];
	   for(i=j-1; i>=l; i--){
	     if(VL[indx[i]]<=a) break;
	     indx[i+1]=indx[i];
	   }
	 indx[i+1]=indxt;
	 }
	 if(jstack==0) break;
	 ir=istack[jstack--];
	 l=istack[jstack--];
      } else{
	 k=(l+ir) >> 1;
	 SWAPE(indx[k], indx[l+1]);
	 if(VL[indx[l]] > VL[indx[ir]]){
	   SWAPE(indx[l], indx[ir])
	 }
	 if(VL[indx[l+1]] > VL[indx[ir]]){
	   SWAPE(indx[l+1], indx[ir])
	 }
	 if(VL[indx[l]] > VL[indx[l+1]]){
	   SWAPE(indx[l],indx[l+1])
	 }
	 i=l+1;
	 j=ir;
	 indxt=indx[l+1];
	 a=VL[indxt];
	 for(;;){
	   do i++; while(VL[indx[i]] < a);
	   do j--; while(VL[indx[j]] > a);
	   if(j<i) break;
	     SWAPE(indx[i], indx[j])
         }
	 indx[l+1]=indx[j];
	 indx[j]=indxt;
	 jstack+=2;
	 if(jstack > NSTACK){
            printf("NSTACK too small in ...");
            exit(0);
         }
	 if(ir-i+1 >= j-l){
	   istack[jstack]=ir;
           istack[jstack-1]=i;
	   ir=j-1;
	 }
	 else{
	   istack[jstack]=j-1;
	   istack[jstack-1]=l;
	   l=i;
	 }
       }
      }
		
      free(istack);
}
