//  *******************************************************************************  //       
//                                                                                   //
//                                 SSRankBoost-Test.c                                //
//                                                                                   //
//   Semi-supervised RankBoost: test module                                          //
//                                                                                   //
//   Author: Massih R. Amini                                                         //
//   Date: 25/03/2016                                                                //
//                                                                                   //
//   Copyright (c) 2016  Massih R. Amini  - All rights reserved                      //
//                                                                                   //
//   This software is available for non-commercial use only. It must not be modified //
//   and distributed without prior permission of the author. The author is not       //
//   responsible for implications from the use of this software.                     //
//                                                                                   //
//  *******************************************************************************  //
#include "defs.h"

int main(int argc, char **argv)
{
  char       input_filename[100], params_filename[200];
  SPARSEVECT *X;
  long int   i, j, *j_etoile, m, d,*Y, K,  MaxTaille;
  LR_PARAM   RBparams;
  double     *theta,*alpha, *theta_etoile, *MaxThetas,*MinThetas,*nu;

  lire_commande_RankBoost(&RBparams,input_filename, params_filename,argc, argv);

  FileSparseVectors(input_filename,&m,&K,&d,&MaxTaille);
  printf("La base d'apprentissage contient %ld exemples, en dimension %ld\n",m,d);


  X=(SPARSEVECT *)Allocation((m+1)*sizeof(SPARSEVECT ));
  for(i=1; i<=m; i++)
     X[i].Att=(FEATURE *)Allocation((MaxTaille+1)*sizeof(FEATURE ));

  Y=(long int *)Allocation((m+1)*sizeof(long int ));

  theta_etoile=    (double *)malloc((RBparams.T+1)*sizeof(double ));
  alpha=    (double *)malloc((RBparams.T+1)*sizeof(double ));
  j_etoile= (long int *)malloc((RBparams.T+1)*sizeof(long int ));

  theta=    (double *)malloc((RBparams.p+1)*sizeof(double));

  MaxThetas=(double *)malloc((d+1)*sizeof(double ));
  MinThetas=(double *)malloc((d+1)*sizeof(double ));
  for(j=1; j<=d; j++){
    MaxThetas[j]=-1.0e8;
    MinThetas[j]=1.0e8;
  }

  nu  = (double *)malloc((m+1)*sizeof(double ));

  ChrgSparseMatrixRB(input_filename, X, MaxThetas, MinThetas, Y, m);

  RankBoost(X, Y, m, d, RBparams, theta, j_etoile, theta_etoile, MaxThetas, MinThetas, alpha, nu, params_filename);
  return 1;
}


void lire_commande_RankBoost(LR_PARAM *RB_input_params, char *fic_apprentissage, char *fic_params, int num_args, char **args)
{
  long int i;

  RB_input_params->T=10;
  RB_input_params->display=1;
  RB_input_params->p=10;

  for(i=1; (i<num_args) && (args[i][0] == '-'); i++){
    switch((args[i])[1]){
      case 't': i++; sscanf(args[i],"%ld",&RB_input_params->T); break;
      case 'p': i++; sscanf(args[i],"%ld",&RB_input_params->p); break;      
      case 'd': i++; sscanf(args[i],"%d",&RB_input_params->display); break;
      case '?': i++; aide();exit(0); break;
      
      default : printf("Option inconnue %s\n",args[i]);Standby();aide();exit(0);
    }
  }
  if((i+1)>=num_args){
    printf("\n ---------------------------- \n Nombre de parametres d'entree insuffisant \n ----------------------------\n\n");
    Standby();
    aide();
    exit(0);
  }
  //printf("%s %s\n",args[i],args[i+1]);
  strcpy(fic_apprentissage, args[i]);
  strcpy(fic_params, args[i+1]);
}

void Standby(){
  printf("\nAide ... \n");
  (void)getc(stdin);
}

void aide(){
  printf("\nL'algorithme RankBoost \n");
  printf("\nAuteur: Massih R. Amini \n");
  printf("Date: 28 juillet 2014\n\n");
  printf("usage: RankBoost [options] fichier_apprentissage fichier_parametres\n\n");
  printf("Options:\n");
  printf("      -t             -> Nombre maximum d'itérations défaut,10)\n");
  printf("      -p             -> Nombre de seuils - decision stumps (défaut, 1)\n");
  printf("      -d             -> Affichage (défaut,1)\n");
  printf("      -?             -> cette aide\n");
  printf("Arguments:\n");
  printf("     fichier_apprentissage -> fichier contenant les exemples\n");
  printf("     fichier_parametres    -> fichier contenant paramètres estimés du modèle\n\n"); 
}

