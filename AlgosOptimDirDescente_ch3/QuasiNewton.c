void qsnewton(FncCout,Grd,Obs,w,epsilon)
double (*FncCout)(double *, DATA); /*@{\color{black!70}// Fonction de co\^{u}t convexe \`{a} minimiser @*/
double* (*Grd)(double *, DATA);    /*@{\color{black!70}// Gradient de la fonction de co\^{u}t}@*/
DATA    Obs;    /*@{\color{black!70}// Structure contenant les informations sur la base  d'entra\^{i}nement}@*/
double  *w;     /*@{\color{black!70}// Vecteur des poids@*/
double epsilon;  /*@{\color{black!70}// Pr\'ecision@*/
{
   long int  i,Epoque=1,j;
   double vTg,invgTBg,gTBg,NewLoss,OldLoss; 
   double *wnew,*oldg,**B,*g,*Bg,*p,*u,*v;

    /*@{\color{black!70}// Allocation des vecteurs et matrice interm\'ediaires}@*/
   B=malloc((Obs.d+1)*sizeof(double *)); 
   if(!B){
     printf("Probleme d'allocation de la matrice des données\n");
     exit(0);
   }
   B[0]=(double *)malloc((size_t)(((Obs.d+1)*(Obs.d+1))*sizeof(double)));
   if(!B[0]){
     printf("Probleme d'allocation de la matrice des données\n");
     exit(0);
   }
  
   for(i=1; i<=Obs.d; i++)
    B[i]=B[i-1]+Obs.d+1;

   oldg= (double *) malloc((Obs.d+1) * sizeof(double ));
   g=(double *) malloc((Obs.d+1) * sizeof(double ));
   v=(double *) malloc((Obs.d+1) * sizeof(double ));
   Bg=(double *) malloc((Obs.d+1) * sizeof(double ));
   wnew=(double *) malloc((Obs.d+1) * sizeof(double ));
   u=(double *) malloc((Obs.d+1) * sizeof(double ));
   p=(double *) malloc((Obs.d+1) * sizeof(double ));


    /*@{\color{black!70}// Calcul de $\hat{\mathcal{L}}(\boldsymbol w^{(0)})$ et $\nabla \hat{\mathcal{L}}(\boldsymbol w^{(0)})$}@*/
   NewLoss=FncCout(w, Obs);
   g  = Grd(w, Obs);

    /*@{\color{black!70}// Initialisation $\mathbf{B}_0\leftarrow \mathbf{Id}_d, \mathbf{p}_0\leftarrow -\nabla \hat{\mathcal{L}}(\boldsymbol w^{(0)})$}@*/
  for (i=0;i<=Obs.d;i++) {
    for (j=0;j<=Obs.d;j++) 
      B[i][j]=0.0; 
    B[i][i]=1.0;
    p[i] = -g[i];
  } 
  OldLoss = NewLoss + 2*epsilon;

  while(fabs(OldLoss-NewLoss) > epsilon*(fabs(OldLoss))){
     OldLoss = NewLoss;

      /*@{\color{black!70}// Calcul du nouveau poids $\boldsymbol w^{(t+1)}\leftarrow \boldsymbol w^{(t)}+\eta_t \mathbf{p}_t$     (Algorithme~\ref{algo:chap4:RechercheLineaire})}@*/
     rchln(FncCout, Grd,w, OldLoss, g, p, wnew, &NewLoss, Obs);  

     /*@{\color{black!70}// Calcul de $\mathbf{v}_{t+1}= \boldsymbol w^{(t+1)}-\boldsymbol w^{(t)}$}@*/
     for (j=0;j<=Obs.d;j++) {
       v[j]=wnew[j]-w[j]; 
       w[j]=wnew[j];
       oldg[j]=g[j]; 
     }
     
     /*@{\color{black!70}// Calcul de $\nabla \hat{\mathcal{L}}(\boldsymbol w^{(t+1)}$}@*/
     g  = Grd(w, Obs);

    /*@{\color{black!70}// Calcul de $\mathbf{g}_{t+1}=\nabla \hat{\mathcal{L}}(\boldsymbol w^{(t+1)})-\nabla \hat{\mathcal{L}}(\boldsymbol w^{(t)})$}@*/
     for(j=0;j<=Obs.d;j++) 
       oldg[j]=g[j]-oldg[j];

      
     /*@{\color{black!70}// Calcul de $\mathbf{B}_t \mathbf{g}_{t+1}$}@*/
     for(j=0;j<=Obs.d;j++) {
       Bg[j]=0.0;
       for (i=0;i<=Obs.d;i++) 
         Bg[j] += B[j][i]*oldg[i];
     }

     /*@{\color{black!70}// Calcul de $\mathbf{v}_{t+1}^T \mathbf{g}_{t+1}$, et de $\mathbf{g}_{t+1}^T\mathbf{B}_t \mathbf{g}_{t+1}$}@*/
    for(vTg=gTBg=0.0,j=0;j<=Obs.d;j++) {
      vTg += v[j]*oldg[j]; 
      gTBg += oldg[j]*Bg[j]; 
    }
    vTg=1.0/vTg;
    invgTBg=1.0/gTBg;
   
   
    /*@{\color{black!70}// $\mathbf{u}_{t+1}=\displaystyle{\frac{\mathbf{v}_{t+1}}{\mathbf{v}_{t+1}^T\mathbf{g}_{t+1}}-\frac{\mathbf{B}_t\mathbf{g}_{t+1}}{\mathbf{g}_{t+1}^T\mathbf{B}_t\mathbf{g}_{t+1}}}$}@*/
    for (j=0;j<=Obs.d;j++) 
      u[j]=vTg*v[j]-invgTBg*Bg[j];
    
    /*@{\color{black!70}// Mise \`a jour de l'estim\'ee de l'inverse de la Hessienne, $\mathbf{B}_{t+1}$}@*/
    /*@{\color{black!70}// Formule de Broyden-Fletcher-Goldfarb-Shanno  (Eq. \ref{eq:BFGS})}@*/  
    for (j=0;j<=Obs.d;j++) 
      for (i=j;i<=Obs.d;i++){
         B[j][i] += vTg*v[j]*v[i] -invgTBg*Bg[j]*Bg[i]+\
         			gTBg*oldg[j]*oldg[i];  
         B[i][j]=B[j][i];
      }
    
     

    /*@{\color{black!70}// Nouvelle direction de descente $\mathbf{p}_{t+1}=-\mathbf{B}_{t+1}\nabla \hat{\mathcal{L}}(\boldsymbol w^{(t+1)})$}@*/
    for(j=0; j<=Obs.d; j++){
      p[j]=0.0;
      for (i=0;i<=Obs.d;i++) 
         p[j] -= B[j][i]*g[i];
    }
    if(!(Epoque%5))
       printf("Epoque:%ld Loss:%lf\n",Epoque,NewLoss);
  
    Epoque++;
 
  }
   free((char *) oldg);
   free((char *) g);
   free((char *) v);
   free((char *) Bg);
   free((char *) wnew);
   free((char *) u);
   free((char *) p);
   free((char *) B[0]);
   free((char *) B);
}
