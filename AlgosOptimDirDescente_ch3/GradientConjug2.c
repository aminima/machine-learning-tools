void grdcnj(FncCout, Grd, Obs, w, epsilon, disp)
double (*FncCout)(double *, DATA); /*@{\color{black!70}// Fonction de co\^{u}t convexe \`a minimiser }@*/
double* (*Grd)(double *, DATA);    /*@{\color{black!70}// Gradient de la fonction de co\^{u}t}@*/
DATA    Obs;    /*@{\color{black!70}// Structure contenant les informations sur la base d'entra\^{i}nement}@*/
double  *w;      // Vecteur des poids
double  epsilon; // Précision
int      disp;   /*@{\color{black!70}// Affichage ou non des valeurs interm\'{e}diaires de la fonction de co\^{u}t}@*/
{
  long int   j, Epoque=0;
  double     *wold, OldLoss, NewLoss, *g, *p, *h, dgg, ngg, beta;

  wold = (double *) malloc((Obs.d+1) * sizeof(double ));
  p    = (double *) malloc((Obs.d+1) * sizeof(double )); 
  g    = (double *) malloc((Obs.d+1) * sizeof(double )); 
  h    = (double *) malloc((Obs.d+1) * sizeof(double ));

  for(j=0; j<=Obs.d; j++)
     wold[j]= 2.0*(rand() / (double) RAND_MAX)-1.0;

  NewLoss = FncCout(wold, Obs);
  OldLoss = NewLoss + 2*epsilon;
  g  = Grd(wold, Obs);

 for(j=0; j<=Obs.d; j++)
    p[j] = -g[j];  // /*@ $\mathbf{p}_0=-\nabla \hat{\mathcal{L}}(\boldsymbol w^{(0)})$ (Eq. \ref{DirDes})@*/
    
    
  while(fabs(OldLoss-NewLoss) > (fabs(OldLoss)*epsilon)) 
  {
    OldLoss = NewLoss;
  /*@/* (Algorithme \ref{algo:chap4:RechercheLineaire}) */@*/ 
    rchln(FncCout, Grd, wold, OldLoss, g, p, w, &NewLoss, Obs); 
    
    h  = Grd(w, Obs); // Nouveau vecteur gradient /*@ $\nabla \hat{\mathcal{L}}(\boldsymbol w^{(t+1)})$ (Eq. \ref{eq:MiseAJour})@*/
 
    for(dgg=0.0, ngg=0.0, j=0; j<=Obs.d; j++){
       dgg+=g[j]*g[j];
       ngg+=h[j]*h[j];   
     }
    
    beta=ngg/dgg; // Formule de Fletcher-Reeves /*@(Eq. \ref{CoeffBeta4})@*/
    for(j=0; j<=Obs.d; j++){
       wold[j]=w[j];
       g[j]=h[j];
       p[j]=-g[j]+beta*p[j]; // Mise à jour de la direction de descente 
    }
    if(!(Epoque%5) && disp)
      printf("Epoque:%ld Loss:%lf\n",Epoque,NewLoss);
    
    Epoque++;
  }
  
  free((char *) wold);
  free((char *) p);
  free((char *) g);
  free((char *) h);
}