#define  ALPHA  1.0e-4
#define  MINETA 1.0e-7

void rchln(FncCout,Grd,wold, Lold, g, p, w, L, Obs)
double (*FncCout)(double *, DATA); /*@{\color{black!70}// Fonction de co\^{u}t convexe \`a minimiser }@*/
double* (*Grd)(double *, DATA);     /*@{\color{black!70} // Gradient de la fonction de co\^{u}t}@*/
double *wold;  /*@{\color{black!70} // Vecteur poids courant,  \`a partir duquel on cherche le nouveau vecteur poids}@*/ 
double Lold;  /*@{\color{black!70}// Valeur de la fonction de co\^{u}t courant}@*/
double *g;     /*@{\color{black!70}// Vecteur gradient} @*/
double *p;     /*@{\color{black!70}// Direction de descente}@*/
double *w;     /*@{\color{black!70}// Nouveau vecteur poids}@*/
double *L;     /*@{\color{black!70}// Nouvelle valeur de la fonction de  co\^{u}t}@*/
DATA Obs;     /*@{\color{black!70} // Structure contenant les informations sur la base d'entra\^{i}nement}@*/
{
  long int j;
  double   a, b, delta, L2, coeff1, coeff2, pente, max; 
  double   eta, eta2, etamin, etatmp;

   /*@{\color{black!70}// Calcul de la pente au point poids actuel}@*/
  for(pente=0.0, j=0; j<=Obs.d; j++)
    pente+=p[j]*g[j];
 
   /*@{\color{black!70}// D\'efinition de la valeur minimale tol\'er\'ee de $\eta$}@*/
   /*@{\color{black!70}// DMAX() est une macro qui renvoie le maximum de deux nombres (d\'efini dans def.h)@*/
  max=0.0;
  for(j=0;j<=Obs.d;j++)
    if(fabs(p[j])>max*DMAX(fabs(wold[j]),1.0))
      max=fabs(p[j])/DMAX(fabs(wold[j]),1.0);
  etamin=MINETA/max;

   /*@{\color{black!70}// Mise \`a jour du vecteur poids pour la plus grande valeur de eta}@*/  
   /*@{\color{black!70}// \`a partir de laquelle on commence la recherche}@*/
  eta=1.0;
  for(j=0;j<=Obs.d;j++)
    w[j]=wold[j]+eta*p[j];

  *L=FncCout(w,Obs);
  
   /*@{\color{black!70}// Boucler tant que la condition d'Armijo n'est pas satisfaite $(Eq.~\ref{eq:ch4:GoldsteinArmijo})$}@*/
  while(*L > (Lold+ALPHA*eta*pente))
  {
    if(eta < etamin)
    {
      for(j=0; j<=Obs.d; j++)
	    w[j]=wold[j];
       // Si le pas trouvé devient trop faible on termine la recherche
      return;
    }
    else
    {
      if(eta==1.0)
        // Le minimiseur du polynome d'interpolation de degré 2 /*@$(Eq.~\ref{eq:ch4:MinimInterpol2})$@*/
        etatmp = -pente/(2.0*(*L-Lold-pente));
      else
      {
        coeff1 = *L-Lold-eta*pente;
        coeff2 = L2-Lold-eta2*pente;
        // Calcul des coefficients du polynôme d'interpolation /*@$(Eq.~\ref{eq:ch4:CoeffInterpol3})$@*/
        a=(coeff1/(eta*eta)-coeff2/(eta2*eta2))/(eta-eta2);
        b=(-eta2*coeff1/(eta*eta)+eta*coeff2/(eta2*eta2))/(eta-eta2);
        if (a != 0.0)
        {
           delta=(b*b)-3.0*a*pente;
           if(delta >= 0.0)
             // Le minimiseur du polynome d'interpolation /*@$(Eq.~\ref{eq:ch4:MinimInterpol3})$@*/
             etatmp=(-b+sqrt(delta))/(3.0*a);
           else
             {printf("rchln: problème d'interpolation");exit(0);}
        }
        else
          etatmp = -pente/(2.0*b);
		
        // /*@$\eta\leq\frac{1}{2}\eta_{p_1}$@*/
        if(etatmp > 0.5*eta)
          etatmp=0.5*eta;
       }
    }
    eta2=eta;
    L2 = *L;
    // /*@$\eta\geq\frac{1}{10}\eta_{p_1}$@*/ - On évite des pas trop faibles
    // DMAX() est une macro qui renvoie le maximum de deux nombres (def.h)

    eta=DMAX(etatmp,0.1*eta);
  
    for(j=0;j<=Obs.d;j++)
      w[j]=wold[j]+eta*p[j];

    *L=FncCout(w,Obs);
  }
}
