
#include "defs.h"


 
int main(argc, argv)
int argc;
char **argv; 
{
    long int   i, j, m, d, T,t;
    double     *Y, **X, *h, **W, *Alpha, *H, Erreur, Precision, Rappel, F, PosPred, PosEffect, PosEffPred;

    if(argc!=3){
        printf("USAGE : AdaBoost-Test Filename ParamsFile\n");
        exit(0);
    }
    FileScan(argv[1],&m,&d);
    FileScan(argv[2],&T,&i);
    printf("AdaBoost sur une base de test contenant %ld exemples en dimension %ld avec %ld classifieurs faibles\n",m,d,T);
    
    h = (double *)  malloc((m+1)*sizeof(double ));
    H = (double *)  malloc((m+1)*sizeof(double ));
    Y  = (double *)  malloc((m+1)*sizeof(double ));
    X  = (double **) malloc((m+1)*sizeof(double *));
    if(!X){
        printf("Probleme d'allocation de la matrice des données\n");
        exit(0);
    }
    X[1]=(double *)malloc((size_t)((m*d+1)*sizeof(double)));
    if(!X[1]){
        printf("Probleme d'allocation de la matrice des données\n");
        exit(0);
    }
    for(i=2; i<=m; i++)
      X[i]=X[i-1]+d;

    Alpha = (double *) malloc((1+T)*sizeof(double ));
    W  = (double **) malloc((1+T)*sizeof(double *));
    if(!W){
        printf("Probleme d'allocation de la matrice des données\n");
        exit(0);
    }
    W[1]=(double *)malloc((size_t)(((d+1)*T)*sizeof(double)));
    if(!W[1]){
        printf("Probleme d'allocation de la matrice des données\n");
        exit(0);
    }
    for(i=2; i<=T; i++)
        W[i]=W[i-1]+d+1;
        
    // Chargement de la matrice des données, procédure définie dans utilitaire.c
    ChrgMatrix(argv[1], m, d, X, Y);
    load_adaboost_params(argv[2],W,Alpha,T,d);
    for(i=1; i<=m; i++)
      H[i]=0.0;
            
    for(t=1; t<=1; t++)
       for(i=1; i<=m; i++){
        /*@$\rhd h_t(\mathbf{x}_i)\leftarrow w^{(t)}_0+\left\langle \boldsymbol w^{(t)},\mathbf{x}_i\right\rangle$@*/
           for(h[i]=W[t][0], j=1; j<=d; j++)
             h[i]+=(W[t][j]*X[i][j]);

           H[i]+=Alpha[t]*h[i];

        }
    
    for(i=1,PosPred=PosEffect=PosEffPred=Erreur=0.0; i<=m; i++){
        if(Y[i]*H[i]<=0.0)
            Erreur+=1.0;
        if(Y[i]==1.0){
            PosEffect++;
            if(H[i]>0.0)
                PosEffPred++;
        }
        if(H[i]>0.0)
            PosPred++;
    }
    
    Erreur/=(double)m;
    Precision=PosEffPred/PosPred;
    Rappel=PosEffPred/PosEffect;
    F=2.0*Precision*Rappel/(Precision+Rappel);
    
    printf("Precision:%lf Rappel:%lf mesure-F:%lf Erreur=%lf\n",Precision,Rappel,F,Erreur);
    return 1;
}


