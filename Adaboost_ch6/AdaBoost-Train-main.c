#include "defs.h"

 

long int Boosting(double **, double **, double *, double *, double *, double **, double *, long int, long int, LR_PARAM);

void lire_commande(LR_PARAM *boost_input_params, char *fic_apprentissage, char *fic_params, \
                   int num_args, char **args)
{
    long int i;
    
    boost_input_params->T=100;
    
    for(i=1; (i<num_args) && (args[i][0] == '-'); i++){
        printf("%s\n",args[i]);
        switch((args[i])[1]){
            case 't': i++; sscanf(args[i],"%ld",&boost_input_params->T); break;
            case '?': i++; aide();exit(0); break;
                
            default : printf("Option inconnue %s\n",args[i]);Standby();aide();exit(0);
        }
    }
    if((i+1)>=num_args){
        printf("\n ---------------------------- \n Nombre de parametres d'entree insuffisant \n ----------------------------\n\n");
        Standby();
        aide();
        exit(0);
    }
    printf("%s %s\n",args[i],args[i+1]);
    strcpy(fic_apprentissage, args[i]);
    strcpy(fic_params, args[i+1]);
}

void Standby(){
    printf("\nAide ... \n");
    (void)getc(stdin);
}

void aide(){
    printf("\nAdaBoost bi-classes\n");
    printf("\nAuteur: Massih R. Amini \n");
    printf("Date: 30 octobre 2013\n\n");
    printf("usage: AdaBoost_Train [options] fichier_apprentissage fichier_parameters\n\n");
    printf("Options:\n");
    printf("      -t             -> Nombre d'itérations maximal (defaut, 100)\n");
    printf("      -d             -> Affichage \n");
    printf("      -?             -> cette aide\n");
    printf("Arguments:\n");
    printf("     fichier_apprentissage -> fichier contenant les exemples d'apprentissage\n");
    printf("     fichier_paramètres    -> fichier contenant les parametres\n\n");
}


int main(argc, argv)
int argc;
char **argv; 
{
    LR_PARAM input_params;
    long int   i, m, d, t;
    double     *Dt, *Y, **X, *Yt, **Xt, *h, **W, *Alpha, *H;
    char input_filename[200], params_filename[200];

    // Lecture de la ligne de commande
    lire_commande(&input_params,input_filename, params_filename,argc, argv);
    // Parcours du fichier d'entrée pour le nombre d'exemples et leur dimension
    // procédure définie dans utilitaire.c
    FileScan(input_filename,&m,&d);
    printf("La base d'apprentissage contient %ld exemples en dimension %ld\n",m,d);
    
    h = (double *)  malloc((m+1)*sizeof(double ));
    H = (double *)  malloc((m+1)*sizeof(double ));
    Dt = (double *)  malloc((m+1)*sizeof(double ));
    Y  = (double *)  malloc((m+1)*sizeof(double ));
    X  = (double **) malloc((m+1)*sizeof(double *));
    if(!X){
        printf("Probleme d'allocation de la matrice des données\n");
        exit(0);
    }
    X[1]=(double *)malloc((size_t)((m*d+1)*sizeof(double)));
    if(!X[1]){
        printf("Probleme d'allocation de la matrice des données\n");
        exit(0);
    }
    for(i=2; i<=m; i++)
      X[i]=X[i-1]+d;
    Yt  = (double *)  malloc((m+1)*sizeof(double ));
    Xt  = (double **) malloc((m+1)*sizeof(double *));
    if(!Xt){
        printf("Probleme d'allocation de la matrice des données\n");
        exit(0);
    }
    Xt[1]=(double *)malloc((size_t)((m*d+1)*sizeof(double)));
    if(!Xt[1]){
        printf("Probleme d'allocation de la matrice des données\n");
        exit(0);
    }
    for(i=2; i<=m; i++)
        Xt[i]=Xt[i-1]+d;
        

    Alpha = (double *) malloc((1+input_params.T)*sizeof(double ));
    W  = (double **) malloc((1+input_params.T)*sizeof(double *));
    if(!W){
        printf("Probleme d'allocation de la matrice des données\n");
        exit(0);
    }
    W[1]=(double *)malloc((size_t)(((d+1)*input_params.T)*sizeof(double)));
    if(!W[1]){
        printf("Probleme d'allocation de la matrice des données\n");
        exit(0);
    }
    for(i=2; i<=input_params.T; i++)
        W[i]=W[i-1]+d+1;
        
    // Chargement de la matrice des données, procédure définie dans utilitaire.c
    ChrgMatrix(input_filename, m, d, X, Y);

    t=Boosting(X,Xt,Y,Yt,Dt,W, Alpha,d,m,input_params);
        
    save_adaboost_params(params_filename,W,Alpha,t,d);
    return 1;
}


