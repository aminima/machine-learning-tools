#include "defs.h"

// Distribution uniforme
void DistUniforme(Dt, m)
double *Dt;
long int m;
{
   int i;
   for (i=1; i<=m; i++)
      Dt[i]=1.0/(double )m;
}

// Echantillonnage avec la méthode du rejet 
void Echantillonnage(Dt, X, Xt, d, m, Y, Yt)
double   *Dt, **X, **Xt;
long int d, m;
double   *Y, *Yt;
{
   long int i, j, U;
   double   V, Maximum,  Majorant;
   

   Maximum=Dt[1];
   for(i=2; i<=m; i++)
     if(Dt[i]>Maximum)
        Maximum=Dt[i];

   Majorant=1.1*Maximum;
   i=1;
   while(i<=m)
   {
      U=(rand()%m)+1;
      V=Majorant*(rand()/ (double) RAND_MAX);
      if(Dt[U]>V)
      {
  	    for(j=1; j<=d; j++)
	      Xt[i][j]=X[U][j];
        Yt[i]=Y[U];
	    i++;
	  }
    }
}

double Pseudo_Loss(Dt, h, Y, m, d)
double   *Dt, *h, *Y;
long int m, d;
{
   long int i;
   double Erreur;

   // /*@$\rhd~\epsilon_t\leftarrow \displaystyle{\sum_{i:f_t(\mathbf{x}_i)\neq y_i} D_t(i)}$@*/
   for(Erreur=0.0, i=1; i<=m; i++)
      if(Y[i]*h[i]<=0.0)
        Erreur+=Dt[i];
  
   return(Erreur);
}

void MiseAJourPoids(Dt, alpha, Y, h, d, m)
double *Dt, *Y, *h, alpha;
long int d, m;
{
   long int i;
   double  Z, sign;
 
   // /*@$\rhd~Z_t\leftarrow \displaystyle{\sum_{i=1}^m D_t(i)e^{-\alpha_t y_i f_t(\mathbf{x}_i)}}$@*/
   for(Z=0.0, i=1; i<=m; i++)
   {
       sign=(h[i]*Y[i]>0.0?1.0:-1.0);
       Dt[i]*=exp(-1.0*alpha*sign);
       Z+=Dt[i];
   }

    // /*@$\rhd~\forall i\in\{1,\ldots,m\}, D_{t+1}(i)\leftarrow \frac{D_t(i)e^{-\alpha_t y_i f_t(\mathbf{x}_i)}}{Z_t}$ (Eq. \ref{eq:ch5:Regle})@*/
    for(i=1; i<=m; i++)
      Dt[i]/=Z;
}


long int Boosting(X, Xt, Y, Yt, Dt, W, Alpha, d, m, input_params)
double   **X, **Xt, *Y, *Yt, *Dt, **W, *Alpha;
long int d, m;
LR_PARAM input_params;
{
    long int i, j, t;
    double *h, *H, err=0.0, ErrEmp, eta=0.01, MaxEpoque=2000;
    LR_PARAM ss_input_params;
    
    h = (double *)  malloc((m+1)*sizeof(double ));
    H = (double *)  malloc((m+1)*sizeof(double ));

    /*@$\rhd~\forall i\in \{1,\ldots,m\}, D_1(i)=\frac{1}{m}$@*/
    DistUniforme(Dt, m);
    Echantillonnage(Dt, X, Xt, d, m, Y, Yt);
    
    for(i=1; i<=m; i++)
        H[i]=0.0;
            
	ss_input_params.eps=1e-3;
	ss_input_params.T=1000;
	ss_input_params.display=0;
    for(t=1; t<=input_params.T && err<0.5; t++)
    {
        perceptron(Xt, Yt, W[t], eta,d,m,MaxEpoque);
     //   RegressionLogistique(Xt,Yt,m,d,W[t],ss_input_params);
        for(i=1; i<=m; i++)
       /*@$\rhd h_t(\mathbf{x}_i)\leftarrow w^{(t)}_0+\left\langle \boldsymbol w^{(t)},\mathbf{x}_i\right\rangle$@*/
         for(h[i]=W[t][0], j=1; j<=d; j++)
            h[i]+=(W[t][j]*X[i][j]);

        err= Pseudo_Loss(Dt, h, Y, m, d);
        if(err<0.5){
         Alpha[t]=0.5*log((1.0-err)/err);
         MiseAJourPoids(Dt, Alpha[t], Y, h, d, m);
         Echantillonnage(Dt, X, Xt, d, m, Y, Yt);
         for(ErrEmp=0.0, i=1; i<=m; i++){
           H[i]+=Alpha[t]*h[i];
           if(Y[i]*H[i]<=0.0)
              ErrEmp+=1.0;
         }
         printf("Epoque=%ld - Epsilon=%lf Alpha=%lf Erreur empirique : %lf\n",t,err,Alpha[t],ErrEmp/(double)m);
       }
    }
    
    
   if(t>input_params.T)
        t--;
    else
        t-=2;
    
    free((char *) H);
    free((char *) h);
    return(t);
}


