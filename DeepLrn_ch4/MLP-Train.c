/* ---------------------------------------------------------------------- *
 * Programme : Algorithme de Cross PMC					  *
 * Auteur    : Massih-Reza AMINI - CopyRight 1998		          *
 * ---------------------------------------------------------------------- */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "defs.h"

#define Emax 10e+10
#define Pui2(x) ((x)*(x))

double drand48();
void PMC_stochastique(DATA, long int, long int *, double ***, LR_PARAM, char *, char *, char *);

void ChargeFicBase(char *FicB, char *FicD, long int m, long int dim, long int class, double **X, double **Y)
{
   long int i,j;
   FILE *fd1, *fd2;
   
   if((fd1=fopen(FicB,"r"))==NULL)
   {
      printf("Erreur d'ouverture du fichier %s\n",FicB);
      exit(0);
   }
   
   if((fd2=fopen(FicD,"r"))==NULL)
   {
      printf("Erreur d'ouverture du fichier %s\n",FicD);
      exit(0);
   }
    
   for(i=1; i<=m; i++)
     for(j=1; j<=dim; j++)
        fscanf(fd1,"%lf",&X[j][i]);
        
   for(i=1; i<=m; i++)
     for(j=1; j<=class; j++)
        fscanf(fd2,"%lf",&Y[j][i]);
     
   fclose(fd1);
   fclose(fd2);
}


void Architecture(NbreCellule, NbreCouche)
long int  *NbreCellule, *NbreCouche;
{
   long int  i;

   printf("Entrez le nombre de couche\n");
   scanf("%ld",NbreCouche);
  // *NbreCouche+=2;
    
   printf("Entrez la dimension du probleme\n");
   scanf("%ld",&NbreCellule[0]);
  
   for(i=1; i<=*NbreCouche; i++)
   {
      printf("Entrez le nombre de cellule de la %ld eme couche cachee\n", i);
	  scanf("%ld",&NbreCellule[i]);
   }
   
   printf("Entrez le nombre de classes\n");
   scanf("%ld",&NbreCellule[*NbreCouche+1]);
}


/* -----------------------------------------------------------------------*
 * Procedure : ModulePrincipale                                           *
 * Fonction  : Main                                                       *
 * Auteur    : Massih-Reza AMINI	                     				  *
 * -----------------------------------------------------------------------*/
int  main(argc, argv)
int  argc;
char **argv; 
{
   DATA         D;
   LR_PARAM     P;
   long int     C, Couche, NbreCellule[20];
   double        **W[20];
   char         FicBase[100], FicDesire[100], FicParam[100];

   long int          i;

   Architecture(NbreCellule, &C);

   printf("* -------------------- Base --------------------- *\n");
   printf("Entrez le cardinal de la base \n");
   scanf("%ld",&D.m);
   printf("Entrez le nom du fichier de la base\n");
   scanf("%s",FicBase);
   printf("Entrez le nom du fichier de la desire\n");
   scanf("%s",FicDesire);

   printf("Entrez le pas Eta : \n");
   scanf("%lf",&P.eta);
   printf("Entrez le nombre de cycles :\n");
   scanf("%ld",&P.T);
   printf("Entrez le nom du fichier des parametres\n");
   scanf("%s",FicParam);

   D.X=(double **)malloc((NbreCellule[0]+1)*sizeof(double *));
   for(i=1; i<=NbreCellule[0]; i++)
	   D.X[i]=(double *)malloc((D.m+1)*sizeof(double ));
   
   D.Y=(double **)malloc((NbreCellule[C+1]+1)*sizeof(double *));
   for(i=1; i<=NbreCellule[C+1]; i++)
	   D.Y[i]=(double *)malloc((D.m+1)*sizeof(double ));
   for(Couche=0; Couche<=C; Couche++){
      W[Couche]=(double **)malloc((1+NbreCellule[Couche+1])*sizeof(double *));
	  for(i=1; i<=NbreCellule[Couche+1]; i++)
		  W[Couche][i]=(double *)malloc((NbreCellule[Couche]+2)*sizeof(double ));
   }
   /* ---------- Initialisation des matrices de travail -------------- */
   printf("Chargement ...\n");
   ChargeFicBase(FicBase, FicDesire, D.m, NbreCellule[0], NbreCellule[C+1], D.X, D.Y);

   printf("*\n ---------------- Appentissage ----------------- *\n");
   PMC_stochastique(D, C, NbreCellule, W, P,FicParam,FicBase,FicDesire);
   return 1;
}

