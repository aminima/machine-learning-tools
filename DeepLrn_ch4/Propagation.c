#include "defs.h"


// Fonction de transfert logistique /*@$\rhd~\mathcal A: x\mapsto\frac{1}{1+e^{-x}}$@*/
double A(x)
double x;
{
   return( 1.0/(1.0+exp((double)-x)) );
}

// Dérivée de la fonction de transfert logistique 
// /*@$\rhd~\forall x, \mathcal A'(x)=z(1-z)$ @*/
// /*@$z=\mathcal A(x)$@*/ sera passée comme paramètre de la fonction
double Aprime(z)
double z;
{
   return( z*(1.0-z) );
}

// Fonction de transfert tangente hyperbolique /*@$\rhd~\mathcal T: x\mapsto\frac{e^{2x}-1}{e^{2x}+1}$@*/
double T(x)
double x;
{
   return( (exp((double)2.0*x)-1.0)/(exp((double)2.0*x)+1.0) );
}

// Dérivée de la fonction de transfert tangente hyperbolique
// /*@$\rhd~\forall x, \mathcal T'(x)=(1-z^2)$ @*/
// /*@$z=\mathcal T(x)$@*/ sera passée comme paramètre de la fonction
double Tprime(z)
double z;
{
   return( 1.0-(z*z) );
}

void Propagation(TrainSet, C, NbCellules, W, ficParam,ficRep)
DATA      TrainSet;     // base d'entrainement 
long int  C;            // nombre de couches cachées 
long int  *NbCellules;  // nombre d'unités sur chaque couche
double    ***W;         // paramètres (poids) du modèle
char      *ficParam;    // nom du fichier des paramètres
char      *ficRep;      // nom du fichier contenant les nouvelles représentations des exemples
{
   long int   i, j,   Av, nc, c, U;
   double     *z[20], aj;
   FILE       *fd, *fd1;

   for(c=0; c<=C+1; c++){
      z[c]=(double *)malloc((1+NbCellules[c])*sizeof(double ));
   }
   
   if((fd=fopen(ficParam,"r"))==NULL){
      printf("Erreur d'ouverture de %s\n",ficParam);
      exit(0);
   }
   
   for(c=0; c<=C; c++)
      for(i=0; i<=NbCellules[c]; i++)
	    for(j=1; j<=NbCellules[c+1]; j++)
	      fscanf(fd,"%lf",&W[c][j][i]);
   fclose(fd);

   if((fd1=fopen(ficRep,"w"))==NULL){
      printf("Erreur d'ouverture de %s\n",ficParam);
      exit(0);
   }
   
   for(U=1; U<=TrainSet.m; U++){
     for(i=1; i<=NbCellules[0]; i++)
	      z[0][i]=TrainSet.X[i][U];

	 for(c=1; c<=C+1; c++)
	 {
	    Av=NbCellules[c-1];
	    nc=NbCellules[c];
	    for(j=1; j<=nc; j++)
	    {
	       // /*@$\rhd~a_j\leftarrow \sum_{i\in Av(j)} w_{ji}z_i$@*/
	       for(aj=W[c-1][j][0], i=1; i<=Av; i++)
		      aj+=W[c-1][j][i]*z[c-1][i];
		   // /*@ $\rhd~z_j\leftarrow \mathcal{T}(a_j)$ @*/  
	       z[c][j]=A(aj);
	    }
	 }
     for(j=1; j<=NbCellules[C+1]; j++)
     	 fprintf(fd1, "%.12lf ",z[c-1][j]);
	    
	 fprintf(fd1,"\n");

   }
   fclose(fd1);
    
   for(c=0; c<=C+1; c++)
      free((char *) z[c]);
}
