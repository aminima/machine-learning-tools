#include "defs.h"
 
// Fonction logistique /*@ $\mathbf{x}\mapsto\frac{1}{1+e^{-\mathbf{x}}}$ @*/
double Logistic(double x)
{
   return (1.0/(1.0+exp(-x)));
}

// Calcul du vecteur gradient /*@ (Eq. \ref{eq:GradLogistique}) @*/
double *Gradient(double *w, double **X, double *y, long int m, long int d) 
{
  double   ps, *g; 
  long int i, j;
  
  g=(double *)malloc((d+1)*sizeof (double));
  for(j=0; j<=d; j++)
    g[j]=0.0;

  for(i=1; i<=m; i++){
     for(ps=w[0],j=1; j<=d; j++) 
       ps+=w[j]*X[i][j];
     g[0]+=(Logistic(y[i]*ps)-1.0)*y[i];
     for(j=1; j<=d; j++)
       g[j]+=(Logistic(y[i]*ps)-1.0)*y[i]*X[i][j];
   }

  for(j=0; j<=d; j++)
	g[j]/=(double ) m;

  return(g);
}

// Calcul de la fonction de cout logistique /*@ (Eq. \ref{eq:CoutLogistique}) @*/
double FoncLoss(double *w, double **X, double *y, long int m, long int d) 
{
  double   S=0.0, ps;
  long int i, j;

  for(i=1; i<=m; i++){
     for(ps=w[0],j=1; j<=d; j++) 
       ps+=w[j]*X[i][j];
     S+= log(1.0+exp(-y[i]*ps));	
  }
  S/=(double ) m;
 
  return (S);
}

void RegressionLogistique(double **X, double *Y, long int m, long int d, double *w, double eps, int display)
{
   //Apprentissage des paramètres du modèle avec la méthode du gradient conjugué
    grdcnj(X, Y, m, d, w, eps, display);

}
