
#include "defs.h"

 

void rchln(wold, Lold, g, p, w, L, X, Y, m, d)
double *wold; /* Vecteur poids courant,  à partir du quel on cherche le nouveau vecteur poids */
double Lold;  // Valeur de la fonction de coût courant
double *g;    // Vecteur gradient 
double *p;    // Direction de descente
double *w;    // Nouveau vecteur poids, 
double *L;    // Nouvelle valeur de la fonction de coût
/* 
  Information permettant de calculer la fonction de coût à un point donné, avec la fonction suivante 
  FoncLoss(w,X,Y,m,d) 
 */
double **X;   // Matrice des données
double *Y;    // Vecteur des classes
long int m;   // Taille de la base d'apprentissage
long int d;   // Dimension du problème
{
  long int j;
  double   a, b, delta, L2, coeff1, coeff2, seuil, pente, sum1, sum2, temp, test; 
  double   eta, eta2, etamin, etatmp;

  for(sum1=0.0, sum2=0.0, j=1; j<=d; j++){
    sum1+=SQR(p[j]);
    sum2+=(w[j]*w[j]);
  }
  seuil=(double )(STPMX*DMAX(sqrt(sum2), (double )d));

  sum1=sqrt(sum1);
  if(sum1 > seuil)
    for(j=1;j<=d;j++)
      p[j]*=seuil/sum1;
  for(pente=0.0, j=1; j<=d; j++)
    pente+=p[j]*g[j];
  test=0.0;
  for(j=1;j<=d;j++)
  {
    temp=fabs(p[j])/DMAX(fabs(wold[j]),1.0);
    if(temp>test)
      test=temp;
  }
  etamin=TOLX/test;
  eta=1.0;

  for(j=1;j<=d;j++)
    w[j]=wold[j]+eta*p[j];

  *L=FoncLoss(w,X,Y,m,d);
  
  
  
  // Boucler tant que la condition d'Armijo n'est pas satisfaite 
  while(*L > (Lold+ALPHA*eta*pente))
  {
    if(eta < etamin)
    {
      for(j=1; j<=d; j++)
	    w[j]=wold[j];
       // Si le pas trouvé devient trop faible on termine la recherche
      return;
    }
    else
    {
      if(eta==1.0)
        // Le minimiseur du polynome d'interpolation de degré 2 
        etatmp = -pente/(2.0*(*L-Lold-pente));
      else
      {
        coeff1 = *L-Lold-eta*pente;
        coeff2 = L2-Lold-eta2*pente;
        // Calcul des coefficients du polynôme d'interpolation de degré 3 
        a=(coeff1/(eta*eta)-coeff2/(eta2*eta2))/(eta-eta2);
        b=(-eta2*coeff1/(eta*eta)+eta*coeff2/(eta2*eta2))/(eta-eta2);
        if (a != 0.0)
        {
           delta=(b*b)-3.0*a*pente;
           if(delta >= 0.0)
             // Le minimiseur du polynome d'interpolation de degré 3 
             etatmp=(-b+sqrt(delta))/(3.0*a);
           else{
             printf("rchln: problème d'interpolation\n");
             exit(0);
           }
        }
        else
          etatmp = -pente/(2.0*b);
		
        
        if(etatmp > 0.5*eta)
          etatmp=0.5*eta;
       }
    }
    eta2=eta;
    L2 = *L;
    eta=FMAX(etatmp,0.1*eta);
  
    for(j=1;j<=d;j++)
      w[j]=wold[j]+eta*p[j];

    *L=FoncLoss(w,X,Y,m,d);
  }
}

