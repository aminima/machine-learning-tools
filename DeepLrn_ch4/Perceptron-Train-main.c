#include "defs.h"
 

int main(int argc, char **argv)
{
  LR_PARAM   input_params;
  long int   i,j, m, d;
  double     *w, *Y, **X, *h, Erreur, Precision, Rappel, F, PosPred, PosEffect, PosEffPred;
  char input_filename[200], params_filename[200];

    srand(time(NULL));
  // Lecture de la ligne de commande
  lire_commande(&input_params,input_filename, params_filename,argc, argv);
  // Parcours du fichier d'entrée pour le nombre d'exemples et leur dimension
  // procédure définie dans utilitaire.c
  FileScan(input_filename,&m,&d);
  printf("La base d'apprentissage contient %ld exemples en dimension %ld\n",m,d);

  Y  = (double *)  malloc((m+1)*sizeof(double ));
  X  = (double **) malloc((m+1)*sizeof(double *));
  if(!X){
    printf("Probleme d'allocation de la matrice des données\n");
    exit(0);
  }
  X[1]=(double *)malloc((size_t)((m*d+1)*sizeof(double)));
  if(!X[1]){
    printf("Probleme d'allocation de la matrice des données\n");
    exit(0);
  }
  for(i=2; i<=m; i++)
    X[i]=X[i-1]+d;

  w  = (double *) malloc((d+1) * sizeof(double ));

  // Chargement de la matrice des données, procédure définie dans utilitaire.c
  ChrgMatrix(input_filename, m, d, X, Y);

  // Algorithme de perceptron 
  perceptron(X, Y, w, m, d, input_params.eta, input_params.T);
   // MarginPerceptron(X, Y, w, m, d, input_params.eta, input_params.T,0.1);
  // Ecriture des paramètres du poids dans le fichier params_filename, procédure définie dans utilitaire.c
    h = (double *)  malloc((m+1)*sizeof(double ));
    for(i=1; i<=m; i++)
    /*@$\rhd h_t(\mathbf{x}_i)\leftarrow w^{(t)}_0+\left\langle \boldsymbol w^{(t)},\mathbf{x}_i\right\rangle$@*/
        for(h[i]=w[0], j=1; j<=d; j++)
            h[i]+=(w[j]*X[i][j]);
    
    
    
    for(i=1,PosPred=PosEffect=PosEffPred=Erreur=0.0; i<=m; i++){
        if(Y[i]*h[i]<=0.0)
            Erreur+=1.0;
        if(Y[i]==1.0){
            PosEffect++;
            if(h[i]>0.0)
                PosEffPred++;
        }
        if(h[i]>0.0)
            PosPred++;
    }
    
    Erreur/=(double)m;
    Precision=PosEffPred/PosPred;
    Rappel=PosEffPred/PosEffect;
    F=2.0*Precision*Rappel/(Precision+Rappel);
    
    printf("Precision:%lf Rappel:%lf mesure-F:%lf Erreur=%lf\n",Precision,Rappel,F,Erreur);
    
    
    free((char *)h);

  save_params(params_filename, w,d);

  return 1;
}



void lire_commande(LR_PARAM *input_params, char *fic_apprentissage, char *fic_params, int num_args, char **args)
{
  long int i;

  input_params->T=5000;
  input_params->eta=0.1;

  for(i=1; (i<num_args) && (args[i][0] == '-'); i++){
    printf("%s\n",args[i]);
    switch((args[i])[1]){
      case 't': i++; sscanf(args[i],"%ld",&input_params->T); break;
      case 'a': i++; sscanf(args[i],"%lf",&input_params->eta); break;
      case '?': i++; aide();exit(0); break;
      
      default : printf("Option inconnue %s\n",args[i]);Standby();aide();exit(0);
    }
  }
  if((i+1)>=num_args){
    printf("\n ---------------------------- \n Nombre de parametres d'entree insuffisant \n ----------------------------\n\n");
    Standby();
    aide();
    exit(0);
  }
  printf("%s %s\n",args[i],args[i+1]);
  strcpy(fic_apprentissage, args[i]);
  strcpy(fic_params, args[i+1]);
}

void Standby(){
  printf("\nAide ... \n");
  (void)getc(stdin);
}

void aide(){
  printf("\nL'algorithme du perceptron\n");
  printf("\nAuteur: Massih R. Amini \n");
  printf("Date: 25 octobre 2013\n\n");
  printf("usage: Perceptron_Train [options] fichier_apprentissage fichier_parameters\n\n");
  printf("Options:\n");
  printf("      -t             -> Nombre itération maximal  (defaut, 5000)\n");
  printf("      -a             -> Pas d'apprentissge  (defaut, 0.1)\n");
  printf("      -?             -> cette aide\n");
  printf("Arguments:\n");
  printf("     fichier_apprentissage -> fichier contenant les exemples d'apprentissage\n");
  printf("     fichier_paramètres    -> fichier contenant les parametres\n\n"); 
}

 


