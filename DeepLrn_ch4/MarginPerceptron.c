 
#include "defs.h"
void MarginPerceptron(X, Y, w, m, d, eta, T, theta)
double **X;
double *Y;
double *w;
long int m;
long int d;
double  eta;
long int T;
double  theta;
{
  long int i, j, t=0;
    double ProdScal;
  // Initialisation du vecteur poids
  for(j=0; j<=d; j++)
    w[j]=0.0;
  
  /* S'il y a moins d'exemples mal classés que le nombre toléré ou 
     qu'on a atteint le nombre maximum d'itérations on sort de la 
     boucle */
  while(t<T)
  {
    i=(rand()%m) + 1;

    for(ProdScal=w[0], j=1; j<=d; j++)
       ProdScal+=w[j]*X[i][j];
    
    // Règle de mise à jour du Perceptron à marge pour un exemple mal classé
    if(Y[i]*ProdScal<= theta){
      w[0]+=eta*Y[i];
      for(j=1; j<=d; j++)
         w[j]+=eta*Y[i]*X[i][j];
    }
    t++;
  }
}
