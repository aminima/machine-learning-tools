#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "defs.h"
 
#define  ALPHA  1.0e-3
#define  TOLX 1.0e-7

#define STPMX 100.0
#define MAXEXP 400.0
#define GTOL 1e-3

long int MaxStr=1e8;

long int LineCnt(char *);
void ChrgMatrix(char *, long int, double **, double *);

void nerror(char *s)
{
  printf("%s\n",s);
  exit(1);
}

double Logistic(double x)
{
	if((-x)>MAXEXP)
		return 0.0;
	else
        return (1.0/(1.0+exp(-x)));
}


double *GradientLoss(double *w, double **X, double *y, long int m, long int d) 
{
  double   ps, *g; 
  long int i, j;
  
  g=(double *)malloc((d+1)*sizeof (double));
  for(j=1; j<=d; j++)
    g[j]=0.0;

  for(i=1; i<=m; i++){
     for(ps=0.0,j=1; j<=d; j++) 
       ps+=w[j]*X[i][j];
     
     for(j=1; j<=d; j++)
       g[j]+=(Logistic(y[i]*ps)-1.0)*y[i]*X[i][j];
   }

  for(j=1; j<=d; j++)//{
	g[j]/=(double ) m;
 //   g[j]+=(2.0*lambda*beta[j]);
 // }

  return(g);
}

double FoncLoss(double *w, double **X, double *y, long int m, long int d) 
{
  double   S=0.0, ps;
  long int i, j;


  for(i=1; i<=m; i++){
     for(ps=0.0,j=1; j<=d; j++) 
       ps+=w[j]*X[i][j];
	 if((-y[i]*ps)>=MAXEXP)
       S+= (-y[i]*ps);
	 else
       S+= log(1.0+exp(-y[i]*ps));	
    // printf("%lf -- %lf\n",-y[i]*ps,S);
  }
  S/=(double ) m;
 
  return (S);
}


void rchln(wold, Lold, g, p, w, L, X, Y, m, d)
double *wold; /* Vecteur poids courant,  � partir du quel on cherche le nouveau vecteur poids */
double Lold;  // Valeur de la fonction de co�t courant
double *g;    // Vecteur gradient 
double *p;    // Direction de descente
double *w;    // Nouveau vecteur poids, 
double *L;    // Nouvelle valeur de la fonction de co�t
/* 
  Information permettant de calculer la fonction de co�t � un point donn�, avec la fonction suivante 
  FoncLoss(w,X,Y,m,d) 
 */
double **X;   // Matrice des donn�es
double *Y;    // Vecteur des classes
long int m;   // Taille de la base d'apprentissage
long int d;   // Dimension du probl�me
{
  long int j;
  double   a, b, delta, L2, coeff1, coeff2, seuil, pente, sum1, sum2, temp, test; 
  double   eta, eta2, etamin, etatmp;

  for(sum1=0.0, sum2=0.0, j=1; j<=d; j++){
    sum1+=SQR(p[j]);
    sum2+=SQR(w[j]);
  }
  seuil=(double )(STPMX*DMAX(sqrt(sum2), (double )d));

  sum1=sqrt(sum1);
  if(sum1 > seuil)
    for(j=1;j<=d;j++)
      p[j]*=seuil/sum1;
  for(pente=0.0, j=1; j<=d; j++)
    pente+=p[j]*g[j];
  test=0.0;
  for(j=1;j<=d;j++)
  {
    temp=fabs(p[j])/DMAX(fabs(wold[j]),1.0);
    if(temp>test)
      test=temp;
  }
  etamin=TOLX/test;
  eta=1.0;

  for(j=1;j<=d;j++)
    w[j]=wold[j]+eta*p[j];

  *L=FoncLoss(w,X,Y,m,d);
  
  
  
  // Boucler tant que la condition d'Armijo n'est pas satisfaite 
  while(*L > (Lold+ALPHA*eta*pente))
  {
    if(eta < etamin)
    {
      for(j=1; j<=d; j++)
	    w[j]=wold[j];
       // Si le pas trouv� devient trop faible on termine la recherche
      return;
    }
    else
    {
      if(eta==1.0)
        // Le minimiseur du polynome d'interpolation de degr� 2 
        etatmp = -pente/(2.0*(*L-Lold-pente));
      else
      {
        coeff1 = *L-Lold-eta*pente;
        coeff2 = L2-Lold-eta2*pente;
        // Calcul des coefficients du polyn�me d'interpolation de degr� 3 
        a=(coeff1/SQR(eta)-coeff2/SQR(eta2))/(eta-eta2);
        b=(-eta2*coeff1/SQR(eta)+eta*coeff2/SQR(eta2))/(eta-eta2);
        if (a != 0.0)
        {
           delta=SQR(b)-3.0*a*pente;
           if(delta >= 0.0)
             // Le minimiseur du polynome d'interpolation de degr� 3 
             etatmp=(-b+sqrt(delta))/(3.0*a);
           else
             nerror("rchln: probl�me d'interpolation");
        }
        else
          etatmp = -pente/(2.0*b);
		
        
        if(etatmp > 0.5*eta)
          etatmp=0.5*eta;
       }
    }
    eta2=eta;
    L2 = *L;
    eta=FMAX(etatmp,0.1*eta);
  
    for(j=1;j<=d;j++)
      w[j]=wold[j]+eta*p[j];

    *L=FoncLoss(w,X,Y,m,d);
  }
}


main(int argc, char **argv)
{
  long int   i, j, l, k, Epoque=1, m, u, d, check=0;
  double     *BETAold, *BETA, min, OldValue=0.0, NewValue, Lambda, Loss, *Y, *g, *p;
  FILE       *fd, *fdw;
  char       NewName[100];
  double     **X, prec, VP,FP, rec, Pos, fmeas, Max=-1.0, x, sum, stepmax;
  double     *h;

  if(argc != 4){
    printf("Usage : LogisticLineSearch <LabeledFile> <Dim> <FicPARAMs>\n");
    exit(0);
  }
   srand((unsigned)time(NULL));

  m=LineCnt(argv[1]);
  sscanf(argv[2],"%ld",&d);
  Y       = (double *)  malloc((m+1)*sizeof(double ));
  X       = (double **) malloc((m+1)*sizeof(double *));
  if(!X)
    nerror("Probleme d'allocation de la matrice des donn�es");

  X[1]=(double *)malloc((size_t)((m*d+1)*sizeof(double)));
  if(!X[1])
    nerror("Probleme d'allocation de la matrice des donn�es");

  for(i=2; i<=m; i++)
    X[i]=X[i-1]+d;
 
  ChrgMatrix(argv[1], m, X, Y);

  sscanf(argv[2],"%ld",&d);
  sscanf(argv[3],"%lf",&Lambda);


  BETA    = (double *) malloc((d+1) * sizeof(double ));
  BETAold = (double *) malloc((d+1) * sizeof(double ));
  p       = (double *) malloc((d+1) * sizeof(double ));
  g       = (double *) malloc((d+1) * sizeof(double ));
  h       = (double *) malloc((d+1) * sizeof(double ));

  for(j=1; j<=d; j++){
  BETAold[j]= 2.0*(rand() / (double) RAND_MAX)-1.0;
  // BETAold[j]=0.0;
  
  // printf("%lf ",BETAold[j]);
  }
  //printf("\n");
  //exit(0);

   //   BETAold[j]= 0.0;
  
  NewValue = FoncLoss(BETAold,  X, Y, m, d);
  printf("#Nb d'exemples s: %ld, Dimension :%ld Loss:%lf\n",m, d, NewValue);
  while(fabs(OldValue-NewValue) > (1e-4*NewValue)){
    OldValue = NewValue;
    g  = GradientLoss(BETAold, X, Y, m, d);

    for(j=1; j<=d; j++)
      p[j] = -g[j];
	
     
     rchln(BETAold, OldValue, g, p, BETA, &NewValue, X, Y, m, d);
      printf("Loss:%lf\n",NewValue);
 
      for(j=1; j<=d; j++)
         BETAold[j]=BETA[j];
  }
}
 
void ChrgMatrix(char *ficname, long int N, double **X, double *Y) 
{
  char       sep[]=": \t", *token, *str;
  long int   i, j, k, d, cls, ind;
  double     x;
  FILE       *fd;


  token=(char *)malloc(MaxStr*sizeof(char));

  if((fd=fopen(ficname,"r"))==NULL){
    perror("Erreur d'ouverture du fichier de donnees");
    exit(0);
  }
  
  i=1;
  
  while(i<=N)
  {
    j=0;
    while((token[j++]=fgetc(fd))!='\n');
    j--;
    token[j]='\0';
    str=strtok(token,sep);
    sscanf(str, "%ld",&cls);
    Y[i]=(double ) cls;
    str=strtok((char *)NULL,sep);
    j=1;
    while(str)
    {
      sscanf(str,"%lf",&X[i][j]);
      str=strtok((char *)NULL,sep);
      j++;
	}
    i++;
  }
 
  fclose(fd);
  free(token);
}

long int LineCnt(char *filename)
{
   FILE *fd;
   long int  ch, prev='\n', lines=0;

   if((fd=fopen(filename,"r"))==NULL){
     perror(filename);
     exit(0);
   }
   while((ch=fgetc(fd)) != EOF){
     if(ch=='\n')
        ++lines;
     prev=ch;
   }   
   fclose(fd);
   if(prev != '\n')
      ++lines;

   return(lines);
}



