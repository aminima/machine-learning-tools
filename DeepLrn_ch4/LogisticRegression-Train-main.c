#include "defs.h"

 
int main(int argc, char **argv)
{
  LR_PARAM   input_params;
  long int   i,j, m, d;
  double     *w, *Y, **X, *h, Erreur, Precision, Rappel, F, PosPred, PosEffect, PosEffPred;
  char input_filename[200], params_filename[200];

  // Lecture de la ligne de commande 
  lire_commande(&input_params,input_filename, params_filename,argc, argv);
  // Parcours du fichier d'entrée pour le nombre d'exemples et leur dimension
  // procédure définie dans utilitaire.c
  FileScan(input_filename,&m,&d);
  printf("La base d'apprentissage contient %ld exemples en dimension %ld\n",m,d);

  Y  = (double *)  malloc((m+1)*sizeof(double ));
  X  = (double **) malloc((m+1)*sizeof(double *));
  if(!X){
    printf("Probleme d'allocation de la matrice des données\n");
    exit(0);
  }
  X[1]=(double *)malloc((size_t)((m*d+1)*sizeof(double)));
  if(!X[1]){
    printf("Probleme d'allocation de la matrice des données\n");
    exit(0);
  }
  for(i=2; i<=m; i++)
    X[i]=X[i-1]+d;

  w  = (double *) malloc((d+1) * sizeof(double ));
  srand(time(NULL));

  for(j=0; j<=d; j++)
     w[j]= 2.0*(rand() / (double) RAND_MAX)-1.0;

  // Chargement de la matrice des données, procédure définie dans utilitaire.c
  ChrgMatrix(input_filename, m, d, X, Y);

  // Modèle régression logisitque entraîné avec un algorithme de descente
  RegressionLogistique(X,Y,m,d,w,input_params.eps, input_params.display);
    
  h = (double *)  malloc((m+1)*sizeof(double ));
  for(i=1; i<=m; i++)
  /*@$\rhd h_t(\mathbf{x}_i)\leftarrow w^{(t)}_0+\left\langle \boldsymbol w^{(t)},\mathbf{x}_i\right\rangle$@*/
     for(h[i]=w[0], j=1; j<=d; j++)
        h[i]+=(w[j]*X[i][j]);
    
    
    
  for(i=1,PosPred=PosEffect=PosEffPred=Erreur=0.0; i<=m; i++){
    if(Y[i]*h[i]<=0.0)
        Erreur+=1.0;
    if(Y[i]==1.0){
        PosEffect++;
        if(h[i]>0.0)
            PosEffPred++;
    }
    if(h[i]>0.0)
        PosPred++;
  }
    
  Erreur/=(double)m;
  Precision=PosEffPred/PosPred;
  Rappel=PosEffPred/PosEffect;
  F=2.0*Precision*Rappel/(Precision+Rappel);
    
  printf("Precision:%lf Rappel:%lf mesure-F:%lf Erreur=%lf\n",Precision,Rappel,F,Erreur);
    
    
  free((char *)h);
  // Ecriture des paramètres du poids dans le fichier params_filename, procédure définie dans utilitaire.c
  save_params(params_filename, w,d);

  return 1;
}



void lire_commande(LR_PARAM *ss_input_params, char *fic_apprentissage, char *fic_params, \
                   int num_args, char **args)
{
  long int i;

  ss_input_params->eps=1e-4;
  ss_input_params->T=10000;
  ss_input_params->display=0;

  for(i=1; (i<num_args) && (args[i][0] == '-'); i++){
    printf("%s\n",args[i]);
    switch((args[i])[1]){
      case 'e': i++; sscanf(args[i],"%lf",&ss_input_params->eps); break;
      case 't': i++; sscanf(args[i],"%ld",&ss_input_params->T); break;
      case 'd': i++; sscanf(args[i],"%d",&ss_input_params->display); break;
      case '?': i++; aide();exit(0); break;
      
      default : printf("Option inconnue %s\n",args[i]);Standby();aide();exit(0);
    }
  }
  if((i+1)>=num_args){
    printf("\n ---------------------------- \n Nombre de parametres d'entree insuffisant \n ----------------------------\n\n");
    Standby();
    aide();
    exit(0);
  }
  printf("%s %s\n",args[i],args[i+1]);
  strcpy(fic_apprentissage, args[i]);
  strcpy(fic_params, args[i+1]);
}

void Standby(){
  printf("\nAide ... \n");
  (void)getc(stdin);
}

void aide(){
  printf("\nRegression logistique avec la méthode du gradient conjugue\n");
  printf("\nAuteur: Massih R. Amini \n");
  printf("Date: 25 octobre 2013\n\n");
  printf("usage: LogisticRegression_Train [options] fichier_apprentissage fichier_parameters\n\n");
  printf("Options:\n");
  printf("      -e             -> précision (defaut, 1e-4)\n");
  printf("      -t             -> Nombre maximum d'itérations (defaut, 10000)\n");
  printf("      -d             -> Affichage (defaut, 0)\n");
  printf("      -?             -> cette aide\n");
  printf("Arguments:\n");
  printf("     fichier_apprentissage -> fichier contenant les exemples d'apprentissage\n");
  printf("     fichier_paramètres    -> fichier contenant les parametres\n\n"); 
}

 


