
#include "defs.h"
void adaline(X, Y, w, m, d, eta, T)
double **X; // La matrice des données
double *Y;  // Le vecteur des classes
double *w;  // Le vecteur des poids
long int m; // La taille de la basse d'apprentissage
long int d; // La dimension des exemples
double eta; // Le pas d'apprentissage
long int T; // Le nombre d'itérations maximum
{
  long int i, j, t=0;
  double h;
  
  // Initialisation du vecteur poids
  srand(time(NULL));
    
  for(j=0; j<=d; j++)
     w[j]= 2.0*(rand() / (double) RAND_MAX)-1.0;

  while(t<T)
  {
    // Choisir un exemple aléatoire /*@$\rhd~ (\mathbf{x}_t,y_t)$ @*/
    i=(rand()%m) + 1;

    
    // /*@$\rhd~h(\mathbf{x}_t)=w_0^{(t)}+\left\langle\bar{\boldsymbol w}^{(t)},\mathbf{x}_t\right\rangle $@*/
    for(h=w[0], j=1; j<=d; j++)
       h+=w[j]*X[i][j];
    
    // Règle de mise à jour de l'adaline /*@$\rhd~ \boldsymbol w^{t+1} \leftarrow \boldsymbol w^{t}+\eta (y_t-h) \mathbf{x}_t$ @*/
    w[0]+=eta*(Y[i]-h);
    for(j=1; j<=d; j++)
       w[j]+=eta*(Y[i]-h)*X[i][j];
    
    t++;
  }
}
