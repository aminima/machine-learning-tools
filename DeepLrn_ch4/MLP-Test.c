/* ---------------------------------------------------------------------- *
 * Programme : Algorithme de Cross PMC					  *
 * Auteur    : Massih-Reza AMINI - CopyRight 1998		          *
 * ---------------------------------------------------------------------- */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "defs.h"

#define Emax 10e+10
#define Pui2(x) ((x)*(x))

double drand48();
void Propagation(DATA, long int, long int *, double ***, char *, char *);

void ChargeFicBase(char *FicB, long int m, long int dim, double **X)
{
   long int i,j;
   FILE *fd1;
   
   if((fd1=fopen(FicB,"r"))==NULL)
   {
      printf("Erreur d'ouverture du fichier %s\n",FicB);
      exit(0);
   }
   
   for(i=1; i<=m; i++)
     for(j=1; j<=dim; j++)
        fscanf(fd1,"%lf",&X[j][i]);
        
   fclose(fd1);
}


void Architecture(NbreCellule, NbreCouche)
long int  *NbreCellule, *NbreCouche;
{
   long int  i;

   printf("Entrez le nombre de couche\n");
   scanf("%ld",NbreCouche);
  // *NbreCouche+=2;
    
   printf("Entrez la dimension du probleme\n");
   scanf("%ld",&NbreCellule[0]);
  
   for(i=1; i<=*NbreCouche; i++)
   {
      printf("Entrez le nombre de cellule de la %ld eme couche cachee\n", i);
	  scanf("%ld",&NbreCellule[i]);
   }
   
   printf("Entrez le nombre de classes\n");
   scanf("%ld",&NbreCellule[*NbreCouche+1]);
}


/* -----------------------------------------------------------------------*
 * Procedure : ModulePrincipale                                           *
 * Fonction  : Main                                                       *
 * Auteur    : Massih-Reza AMINI	                     				  *
 * -----------------------------------------------------------------------*/
int  main(argc, argv)
int  argc;
char **argv; 
{
   DATA         D;
   long int     Couche, C, NbreCellule[20];
   double        **W[20];
   char         FicBase[100], FicParam[100], FicPred[100];

   long int          i;

   Architecture(NbreCellule, &C);

   printf("* -------------------- Base --------------------- *\n");
   printf("Entrez le cardinal de la base \n");
   scanf("%ld",&D.m);
   printf("Entrez le nom du fichier de la base\n");
   scanf("%s",FicBase);
 
   printf("Entrez le nom du fichier des parametres\n");
   scanf("%s",FicParam);
   printf("Entrez le nom du fichier des predictions\n");
   scanf("%s",FicPred);

   D.X=(double **)malloc((NbreCellule[0]+1)*sizeof(double *));
   for(i=1; i<=NbreCellule[0]; i++)
	   D.X[i]=(double *)malloc((D.m+1)*sizeof(double ));
   for(Couche=0; Couche<=C; Couche++){
      W[Couche]=(double **)malloc((1+NbreCellule[Couche+1])*sizeof(double *));
	  for(i=1; i<=NbreCellule[Couche+1]; i++)
		  W[Couche][i]=(double *)malloc((NbreCellule[Couche]+2)*sizeof(double ));
   }
   
    /* ---------- Initialisation des matrices de travail -------------- */
   printf("Chargement ...\n");
   ChargeFicBase(FicBase, D.m, NbreCellule[0], D.X);

   printf("*\n ---------------- Propagation ----------------- *\n");
   Propagation(D, C, NbreCellule, W, FicParam,FicPred);
   return 1;
}

