#include "defs.h"


// Fonction de transfert logistique /*@$\rhd~\mathcal A: x\mapsto\frac{1}{1+e^{-x}}$@*/
double A(x)
double x;
{
   return( 1.0/(1.0+exp((double)-x)) );
}

// Dérivée de la fonction de transfert logistique 
// /*@$\rhd~\forall x, \mathcal A'(x)=z(1-z)$ @*/
// /*@$z=\mathcal A(x)$@*/ sera passée comme paramètre de la fonction
double Aprime(z)
double z;
{
   return( z*(1.0-z) );
}

// Fonction de transfert tangente hyperbolique /*@$\rhd~\mathcal T: x\mapsto\frac{e^{2x}-1}{e^{2x}+1}$@*/
double T(x)
double x;
{
   return( (exp((double)2.0*x)-1.0)/(exp((double)2.0*x)+1.0) );
}

// Dérivée de la fonction de transfert tangente hyperbolique
// /*@$\rhd~\forall x, \mathcal T'(x)=(1-z^2)$ @*/
// /*@$z=\mathcal T(x)$@*/ sera passée comme paramètre de la fonction
double Tprime(z)
double z;
{
   return( 1.0-(z*z) );
}

void PMC_stochastique(TrainSet, C, NbCellules, W, params,ficParam,ficRep,ficDes)
DATA      TrainSet;     // base d'entrainement 
long int  C;            // nombre de couches cachées 
long int  *NbCellules;  // nombre d'unités sur chaque couche
double    ***W;         // paramètres (poids) du modèle
LR_PARAM  params;       // paramètres du programme 
char      *ficParam;    // nom du fichier des paramètres
char      *ficRep;      // nom du fichier contenant les nouvelles représentations des exemples
char      *ficDes;      // nom du fichier désiré
{
   long int   i, j, q, l,k, Av, Ap,nc, c, t=1, U;
   double     *delta[20], *z[20], aj, sommeDW, NEWe, OLDe, SumE;
   FILE       *fd;

   srand(time(NULL));

   for(c=0; c<=C+1; c++)
      z[c]=(double *)malloc((1+NbCellules[c])*sizeof(double ));
   for(c=0; c<=C+1; c++)
      delta[c]=(double *)malloc((1+NbCellules[c])*sizeof(double ));
   
     // Initialisation aléatoire des paramètres du modèle
   for(c=0; c<=C; c++)
      for(i=0; i<=NbCellules[c]; i++)
	    for(j=1; j<=NbCellules[c+1]; j++)
	       W[c][j][i]=2.0*(rand() / (double) RAND_MAX)-1.0;
   
   SumE=NEWe=0.0;
   OLDe=-1.0;

   while((fabs(NEWe-OLDe)>(double )t*params.eps) && (t<=params.T))
   {
     OLDe=NEWe;
	 
	 // Tirage aléatoire de l'indice d'un exemple d'entrainement
	 U=(rand()%TrainSet.m) + 1;
	 // /*@$\rhd~\forall i, z_i=x_i$ @*/pour les cellules de la couche d'entrée
	 for(i=1; i<=NbCellules[0]; i++)
	   z[0][i]=TrainSet.X[i][U];
  
   	 /* ------------------------------------------------- *
	  *                Phase de propagation 	          *
	  * ------------------------------------------------- */
	 for(c=1; c<=(C+1); c++)
	 {
	    Av=NbCellules[c-1];
	    nc=NbCellules[c];
	    for(j=1; j<=nc; j++)
	    {
	       // /*@$\rhd~a_j\leftarrow \sum_{i\in Av(j)} w_{ji}z_i$@*/
	       for(aj=W[c-1][j][0], i=1; i<=Av; i++)
		      aj+=W[c-1][j][i]*z[c-1][i];
		   // /*@ $\rhd~z_j\leftarrow \mathcal{T}(a_j)$ @*/  
	       z[c][j]=A(aj);
	    }
	 }
	 
	 k=NbCellules[C+1];
	 // /*@$\rhd~\forall i, z_i=h_i$ pour les cellules de la couche de sortie@*/
	 SumE=0.0;
	 for(q=1; q<=k; q++)
	 {
	    SumE+=0.5*pow(z[C+1][q]-TrainSet.Y[q][U], 2);
	    // /*@$\rhd~\delta_q\leftarrow \mathcal{A}'(a_q)\times(h_q-y_q)$@*/
		delta[C+1][q]=Aprime(z[C+1][q])*(z[C+1][q]-TrainSet.Y[q][U]);
	 }
	 NEWe+=SumE;///(double) t;

	 /* ------------------------------------------------------ *
	  *  Phase de RetroPropagation et de correction des poids  *
	  * ------------------------------------------------------ */
	 for(c=C; c>=0; c--)
	 {
	    Ap=NbCellules[c+1];
	    nc=NbCellules[c];
	    for(j=1; j<=nc; j++)
	    {
		  // /*@$\rhd~\delta_j\leftarrow \mathcal{A}'(a_j)\sum_{l\in Ap(j)}\delta_l w_{lj}$ (\'equation \ref{eq:PMC:chaine2})@*/
		  for(sommeDW=0.0, l=1; l<=Ap; l++)
		     sommeDW+=delta[c+1][l]*W[c][l][j];
		  delta[c][j]=Aprime(z[c][j])*sommeDW;
		  
   	       /* ------------------------------------------------- *
	        *        descente de gradient stochastique          *
	        * ------------------------------------------------- */
		  // /*@$\rhd~\Delta_{lj}\leftarrow -\eta\delta_l z_j$ (\'equation \ref{eq:PMC:MiseAJour}) @*/
		  for(l=1; l<=Ap; l++)
		     W[c][l][j]-=(params.eta*delta[c+1][l]*z[c][j]);
	    }
        for(l=1; l<=Ap; l++)
	       W[c][l][0]-=(params.eta*delta[c+1][l]);
	 }

     if (!(t%5))
     {
       printf("Epoque : %ld \n",t);
       printf("Delta erreur (en-ligne) = %lf\n",fabs(NEWe-OLDe)/(double )t);
     }
     
      t++;
   } /* fin while((fabs(NEWe-OLDe)>params.eps) && (t<=params.T)) */
	    
	    
   if((fd=fopen(ficParam,"w"))==NULL){
      printf("Erreur d'ouverture de %s\n",ficParam);
      exit(0);
   }
   
   for(c=0; c<=C; c++){
      for(i=0; i<=NbCellules[c]; i++){
	    for(j=1; j<=NbCellules[c+1]; j++)
	      fprintf(fd,"%.24lf ",W[c][j][i]);
        fprintf(fd,"\n");
      }
      fprintf(fd,"\n");
    }
   fclose(fd);


    

   for(c=0; c<=C+1; c++)
      free((char *) z[c]);
   for(c=1; c<=C+1; c++)
      free((char *)delta[c]);

}
