
#include "defs.h"

void CEMss_NaiveBayes(Xl, EtiqCls, Xu, m, u, d, K, lambda, epsilon, T, display, theta, pi)
SPARSEVECT *Xl;      //    Représentation creuse des exemples étiquetés
long int   *EtiqCls; // Vecteur de classe
SPARSEVECT *Xu;      //  Représentation creuse des exemples non-étiquetés
long int m;          // Taille de la base étiquetée
long int u;          // Taille de la base non-étiquetée
long int d;          // Dimension du problème
long int K;          // Nombre de classes
double lambda;       // Facteur d'implication des données non-étiquetées
double epsilon;      //  Précision recherchée
long int T;          // Nombre d'époques maximal
int display;         // Option d'affichage
double **theta;      // Paramètres du modèle
double *pi;          // Paramètres du modèle
{
  long int Epoque=0, i, j, k, maxCls;
  double   denominateur, logProbaJointe, max, Lnew, Lold;
  Lnew=1.0;
  Lold=0.0;

  // Initialisation des paramètres du modèle sur les données étiquetées 

  // /*@ $\rhd ~\forall k, \pi_k=\frac{\displaystyle{\sum_{i=1}^m z_{ik}}}{m}$ (Eq. \ref{eq:pik} avec $\lambda=0$) @*/
  for(k=1; k<=K; k++){
    pi[k]=0.0;
    for(i=1; i<=m; i++)
      if(EtiqCls[i]==k)
        pi[k]++;
    pi[k]/=(double )m;
}
  // /*@ $\rhd ~\forall k, \forall j, \theta_{j\mid k}=\frac{\displaystyle{\sum_{i=1}^m z_{ik} n_{i,j}}}{\displaystyle{\sum_{i=1}^m z_{ik} \sum_{j=1}^d n_{i,j}}}$ (Eq. \ref{eq:thetajk2} avec $\lambda=0$) @*/
   for(k=1; k<=K; k++){
     for(j=1; j<=d; j++)
       theta[j][k]=1.0;
     denominateur=0.0;
	 for(i=1; i<=m; i++)
        if(EtiqCls[i]==k)
           for(j=1; j<=Xl[i].N; j++){
		   	  theta[Xl[i].Att[j].ind][k]+=Xl[i].Att[j].val;
		      denominateur+=Xl[i].Att[j].val;
           }
     for(j=1;  j<=d; j++)
       theta[j][k]/=(denominateur+(double ) d);
	 
  }
  while((fabs(Lnew-Lold)>=epsilon) && (Epoque<T))
  {  
     Lold=Lnew;
     // Etapes E et C de l'algorithme /*@ (\ref{algo:CEMSemiSup}) @*/
     
     for(i=1; i<=u; i++){
        // Logarithme de la probabilité jointe sans les termes multinomiaux /*@ (Eq. \ref{eq:ProbaJointe}) @*/
		logProbaJointe=log(pi[1]);
        for(j=1; j<=Xu[i].N; j++)
              logProbaJointe+=Xu[i].Att[j].val*log(theta[Xu[i].Att[j].ind][1]);
        for(k=2, max=logProbaJointe, maxCls=1; k<=K; k++){
            logProbaJointe=log(pi[k]);
        for(j=1; j<=Xu[i].N; j++)
              logProbaJointe+=Xu[i].Att[j].val*log(theta[Xu[i].Att[j].ind][k]);

            if(max<logProbaJointe){
              max=logProbaJointe;
              maxCls=k;
            }
        }
        // Affectation de l'exemple à la classe d'après la règle de l'équation /*@ (\ref{eq:etapeC}) @*/
        EtiqCls[m+i]=maxCls;
     }
     
     // Etape M de l'algorithme /*@ \ref{algo:CEMSemiSup} @*/
    
    // /*@ $\rhd~\forall k, \pi_k = \frac{\displaystyle{\sum_{i=1}^m z_{ik}+\lambda \sum_{i=m+1}^{m+u} \tilde z_{ik}}}{m +\lambda u}$ (Eq. \ref{eq:pik}) @*/
    for(k=1; k<=K; k++){
       pi[k]=0.0;
       for(i=1; i<=m; i++)
         if(EtiqCls[i]==k)
           pi[k]++;
           
        for(i=1; i<=u; i++)
           if(EtiqCls[m+i]==k)
              pi[k]+=lambda;
       pi[k]/=((double )m + lambda*((double) u));
    }
  
    // /*@ $\rhd~\forall j, \forall k, \theta_{j\mid k}=\frac{\displaystyle{\sum_{i=1}^m z_{ik}n_{i,j}+\lambda \sum_{i=m+1}^{m+u} \tilde z_{ik}n_{i,j}+1}}{\displaystyle{\sum_{i=1}^m z_{ik}\sum_{j=1}^d n_{i,j}+\lambda \sum_{i=m+1}^{m+u} \tilde z_{ik}\sum_{j=1}^d n_{i,j}+d}}$ (Eq. \ref{eq:thetajk2}) @*/
   for(k=1; k<=K; k++){
     for(j=1; j<=d; j++)
       theta[j][k]=1.0;
     denominateur=0.0;
	 for(i=1; i<=m; i++)
        if(EtiqCls[i]==k)
           for(j=1; j<=Xl[i].N; j++){
		   	  theta[Xl[i].Att[j].ind][k]+=Xl[i].Att[j].val;
		      denominateur+=Xl[i].Att[j].val;
           }
	 for(i=1; i<=u; i++)
        if(EtiqCls[m+i]==k)
           for(j=1; j<=Xu[i].N; j++){
		   	  theta[Xu[i].Att[j].ind][k]+=(lambda*Xu[i].Att[j].val);
		      denominateur+=(lambda*Xu[i].Att[j].val);
           }
     for(j=1; j<=d; j++)
       theta[j][k]/=(denominateur+(double ) d);
   }
   for(i=1, Lnew=0.0; i<=m; i++){
        Lnew+=log(pi[EtiqCls[i]]);
           for(j=1; j<=Xl[i].N; j++)
              Lnew+=(Xl[i].Att[j].val*log(theta[Xl[i].Att[j].ind][EtiqCls[i]]));
     }
     for(i=1; i<=u; i++){
        Lnew+=(lambda*log(pi[EtiqCls[m+i]]));
           for(j=1; j<=Xu[i].N; j++)
              Lnew+=(lambda*Xu[i].Att[j].val*log(theta[Xu[i].Att[j].ind][EtiqCls[m+i]]));
     }
     if(!(Epoque%5) && display)
          printf("Epoque:%ld LogVC:%lf\n",Epoque, Lnew);

     Epoque++;
  }

}

