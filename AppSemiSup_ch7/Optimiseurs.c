/*  ***********************************************************************************  //
//                              Optimiseurs.c                                            //                                                                                       //
// rchln : Recherche linéaire (line search)                                              // 
// qsnewton : Méthode de quasi-Newton, variante Broyden-Fletcher-Goldfarb-Shanno (BFGS)
// grdcnj: Minimisation d'une fonction de coût suivant la méthode du gradient conjugué   //                                    
//
//   Author: Massih R. Amini                                                             //
//   Date: 25/10/2013                                                                    //
//
//  ************************************************************************************ */

#include "defs.h"

void rchln(FncCout,Grd,wold, Lold, g, p, w, L, Obs)
double (*FncCout)(double *, DATA);
              // Fonction de coût convexe à minimiser 
double* (*Grd)(double *, DATA);
			  // Grd de la fonction de coût
double *wold; // Vecteur poids courant,  à partir du quel on cherche le nouveau vecteur poids 
double Lold;  // Valeur de la fonction de coût courant
double *g;    // Vecteur gradient 
double *p;    // Direction de descente
double *w;    // Nouveau vecteur poids, 
double *L;    // Nouvelle valeur de la fonction de coût
/* 
  Information permettant de calculer la fonction de coût à un point donné, 
  Ce calcul est fait par la fonction FncCout(w,X,Y,m,d) 
 */
DATA Obs; // Structure contenant les données et les informations sur la base d'entrainement
{
  long int j;
  double   a, b, delta, L2, coeff1, coeff2, pente, max; 
  double   eta, eta2, etamin, etatmp;

  // Calcul de la pente au point poids actuel
  for(pente=0.0, j=0; j<=Obs.d; j++)
    pente+=p[j]*g[j];
 
  // Définition de la valeur minimale tolérée de /*@$\eta$@*/
  // DMAX() est un macro qui renvoie le maximum de deux nombres (défini dans def.h)
  max=0.0;
  for(j=0;j<=Obs.d;j++)
    if(fabs(p[j])>max*DMAX(fabs(wold[j]),1.0))
      max=fabs(p[j])/DMAX(fabs(wold[j]),1.0);
  etamin=MINETA/max;

  // Mise à jour du vecteur poids pour la plus grande valeur de eta  
  // à partir de laquelle on commence la recherche
  eta=1.0;
  for(j=0;j<=Obs.d;j++)
    w[j]=wold[j]+eta*p[j];

  *L=FncCout(w,Obs);
  
  // Boucler tant que la condition d'Armijo n'est pas satisfaite /*@$(Eq.~\ref{eq:ch4:GoldsteinArmijo})$@*/
  while(*L > (Lold+ALPHA*eta*pente))
  {
    if(eta < etamin)
    {
      for(j=0; j<=Obs.d; j++)
	    w[j]=wold[j];
       // Si le pas trouvé devient trop faible on termine la recherche
      return;
    }
    else
    {
      if(eta==1.0)
        // Le minimiseur du polynome d'interpolation de degré 2 /*@$(Eq.~\ref{eq:ch4:MinimInterpol2})$@*/
        etatmp = -pente/(2.0*(*L-Lold-pente));
      else
      {
        coeff1 = *L-Lold-eta*pente;
        coeff2 = L2-Lold-eta2*pente;
        // Calcul des coefficients du polynôme d'interpolation de degré 3 /*@$(Eq.~\ref{eq:ch4:CoeffInterpol3})$@*/
        a=(coeff1/(eta*eta)-coeff2/(eta2*eta2))/(eta-eta2);
        b=(-eta2*coeff1/(eta*eta)+eta*coeff2/(eta2*eta2))/(eta-eta2);
        if (a != 0.0)
        {
           delta=(b*b)-3.0*a*pente;
           if(delta >= 0.0)
             // Le minimiseur du polynome d'interpolation de degré 3 /*@$(Eq.~\ref{eq:ch4:MinimInterpol3})$@*/
             etatmp=(-b+sqrt(delta))/(3.0*a);
           else
             {printf("rchln: problème d'interpolation");exit(0);}
        }
        else
          etatmp = -pente/(2.0*b);
		
        // /*@$\eta\leq\frac{1}{2}\eta_{p_1}$@*/
        if(etatmp > 0.5*eta)
          etatmp=0.5*eta;
       }
    }
    eta2=eta;
    L2 = *L;
    // /*@$\eta\geq\frac{1}{10}\eta_{p_1}$@*/ - On évite des pas trop faibles
    // DMAX() est un macro qui renvoie le maximum de deux nombres (défini dans def.h)

    eta=DMAX(etatmp,0.1*eta);
  
    for(j=0;j<=Obs.d;j++)
      w[j]=wold[j]+eta*p[j];

    *L=FncCout(w,Obs);
  }
}


void qsnewton(FncCout,Grd,Obs,w,epsilon)
double (*FncCout)(double *, DATA);
                // Fonction de coût convexe à minimiser 
double* (*Grd)(double *, DATA);
				// Grd de la fonction de coût
DATA    Obs;
double  *w;     // Vecteur des poids
double epsilon; // Précision
{
   long int  i,Epoque=1,j;
   double vTg,invgTBg,gTBg,NewLoss,OldLoss; 
   double *wnew,*oldg,**B,*g,*Bg,*p,*u,*v;

   // Allocation des vecteurs et matrice intermédiaires
   B=malloc((Obs.d+1)*sizeof(double *)); 
   if(!B){
     printf("Probleme d'allocation de la matrice des données\n");
     exit(0);
   }
   B[0]=(double *)malloc((size_t)(((Obs.d+1)*(Obs.d+1))*sizeof(double)));
   if(!B[0]){
     printf("Probleme d'allocation de la matrice des données\n");
     exit(0);
   }
  
   for(i=1; i<=Obs.d; i++)
    B[i]=B[i-1]+Obs.d+1;

   oldg= (double *) malloc((Obs.d+1) * sizeof(double ));
   g=(double *) malloc((Obs.d+1) * sizeof(double ));
   v=(double *) malloc((Obs.d+1) * sizeof(double ));
   Bg=(double *) malloc((Obs.d+1) * sizeof(double ));
   wnew=(double *) malloc((Obs.d+1) * sizeof(double ));
   u=(double *) malloc((Obs.d+1) * sizeof(double ));
   p=(double *) malloc((Obs.d+1) * sizeof(double ));


   // Calcul de /*@$\mathcal{L}(\boldsymbol w^{(0)})$ et $\nabla \mathcal{L}(\boldsymbol w^{(0)})$@*/
   NewLoss=FncCout(w, Obs);
   g  = Grd(w, Obs);

   // Initialisation /*@$\mathbf{B}_0\leftarrow \mathbf{Id}_d, \mathbf{p}_0\leftarrow -\nabla \mathcal{L}(\boldsymbol w^{(0)})$@*/
  for (i=0;i<=Obs.d;i++) {
    for (j=0;j<=Obs.d;j++) 
      B[i][j]=0.0; 
    B[i][i]=1.0;
    p[i] = -g[i];
  } 
  OldLoss = NewLoss + 2*epsilon;

  while(fabs(OldLoss-NewLoss) > epsilon*(fabs(OldLoss))){
     OldLoss = NewLoss;

     // Calcul du nouveau poids /*@$\boldsymbol w^{(t+1)}\leftarrow \boldsymbol w^{(t)}+\eta_t \mathbf{p}_t$@*/
     rchln(FncCout, Grd,w, OldLoss, g, p, wnew, &NewLoss, Obs); // Recherche linéaire /*@(Algorithme \ref{algo:chap4:RechercheLineaire})@*/

    // Calcul de /*@$\mathbf{v}_{t+1}= \boldsymbol w^{(t+1)}-\boldsymbol w^{(t)}$@*/
     for (j=0;j<=Obs.d;j++) {
       v[j]=wnew[j]-w[j]; 
       w[j]=wnew[j];
       oldg[j]=g[j]; 
     }
     
    // Calcul de /*@$\nabla \mathcal{L}(\boldsymbol w^{(t+1)}$@*/
     g  = Grd(w, Obs);

    // Calcul de /*@$\mathbf{g}_{t+1}=\nabla \mathcal{L}(\boldsymbol w^{(t+1)})-\nabla \mathcal{L}(\boldsymbol w^{(t)})$@*/
     for(j=0;j<=Obs.d;j++) 
       oldg[j]=g[j]-oldg[j];

      
     // Calcul de /*@$\mathbf{B}_t \mathbf{g}_{t+1}$@*/
     for(j=0;j<=Obs.d;j++) {
       Bg[j]=0.0;
       for (i=0;i<=Obs.d;i++) 
         Bg[j] += B[j][i]*oldg[i];
     }

     // Calcul de /*@$\mathbf{v}_{t+1}^T \mathbf{g}_{t+1}$, et de $\mathbf{g}_{t+1}^T\mathbf{B}_t \mathbf{g}_{t+1}$@*/
    for(vTg=gTBg=0.0,j=0;j<=Obs.d;j++) {
      vTg += v[j]*oldg[j]; 
      gTBg += oldg[j]*Bg[j]; 
    }
    vTg=1.0/vTg;
    invgTBg=1.0/gTBg;
   
   
    // /*@$\mathbf{u}_{t+1}=\displaystyle{\frac{\mathbf{v}_{t+1}}{\mathbf{v}_{t+1}^T\mathbf{g}_{t+1}}-\frac{\mathbf{B}_t\mathbf{g}_{t+1}}{\mathbf{g}_{t+1}^T\mathbf{B}_t\mathbf{g}_{t+1}}}$ @*/
    for (j=0;j<=Obs.d;j++) 
      u[j]=vTg*v[j]-invgTBg*Bg[j];
    
    // Mise à jour de l'estimée de l'inverse de la Hessienne, /*@$\mathbf{B}_{t+1}$@*/
    // Formule de Broyden-Fletcher-Goldfarb-Shanno  /*@ (Eq. \ref{eq:BFGS})@*/  
    for (j=0;j<=Obs.d;j++) 
      for (i=j;i<=Obs.d;i++){
         B[j][i] += vTg*v[j]*v[i] -invgTBg*Bg[j]*Bg[i]+gTBg*oldg[j]*oldg[i];  
         B[i][j]=B[j][i];
      }
    

    // Nouvelle direction de descente /*@$\mathbf{p}_{t+1}=-\mathbf{B}_{t+1}\nabla \mathcal{L}(\boldsymbol w^{(t+1)})$@*/
    for(j=0; j<=Obs.d; j++){
      p[j]=0.0;
      for (i=0;i<=Obs.d;i++) 
         p[j] -= B[j][i]*g[i];
    }
    if(!(Epoque%5))
       printf("Epoque:%ld Loss:%lf\n",Epoque,NewLoss);
  
    Epoque++;
 
  }
   free((char *) oldg);
   free((char *) g);
   free((char *) v);
   free((char *) Bg);
   free((char *) wnew);
   free((char *) u);
   free((char *) p);
   free((char *) B[0]);
   free((char *) B);
}


void grdcnj(FncCout, Grd, Obs, w, epsilon, disp)
double (*FncCout)(double *, DATA);
                // Fonction de coût convexe à minimiser 
double* (*Grd)(double *, DATA);
				// Grd de la fonction de coût
DATA    Obs;    // Structure contenant les exemples 
double  *w;     // Vecteur des poids
double  epsilon; // Précision
int      disp;    // Affichage ou non des valeurs intermédiaires de la fonction de cout
{
  long int   j, Epoque=0;
  double     *wold, OldLoss, NewLoss, *g, *p, *h, dgg, ngg, beta;

  wold = (double *) malloc((Obs.d+1) * sizeof(double ));
  p    = (double *) malloc((Obs.d+1) * sizeof(double )); 
  g    = (double *) malloc((Obs.d+1) * sizeof(double )); 
  h    = (double *) malloc((Obs.d+1) * sizeof(double ));

  for(j=0; j<=Obs.d; j++)
     wold[j]= 2.0*(rand() / (double) RAND_MAX)-1.0;

  NewLoss = FncCout(wold, Obs);
  OldLoss = NewLoss + 2*epsilon;
  g  = Grd(wold, Obs);

 for(j=0; j<=Obs.d; j++)
    p[j] = -g[j];  // /*@ $\mathbf{p}_0=-\nabla \mathcal L(\boldsymbol w^{(0)})$ (Eq. \ref{DirDes})@*/
    
    
  while(fabs(OldLoss-NewLoss) > (fabs(OldLoss)*epsilon)) 
  {
    OldLoss = NewLoss;
    
    rchln(FncCout, Grd, wold, OldLoss, g, p, w, &NewLoss, Obs); // Recherche linéaire /*@(Algorithme \ref{algo:chap4:RechercheLineaire})@*/
    
    h  = Grd(w, Obs); // Nouveau vecteur gradient /*@ $\nabla \mathcal{L}(\boldsymbol w^{(t+1)})$ (Eq. \ref{eq:MiseAJour})@*/
 
    for(dgg=0.0, ngg=0.0, j=0; j<=Obs.d; j++){
       dgg+=g[j]*g[j];
       ngg+=h[j]*h[j];   
   //    ngg+=h[j]*(h[j]-g[j]); // Pour le calcul de formule de Ribière-Polak  /*@(Eq. \ref{CoeffBeta3})@*/
     }
    
    beta=ngg/dgg; // Formule de Fletcher-Reeves /*@(Eq. \ref{CoeffBeta4})@*/
    for(j=0; j<=Obs.d; j++){
       wold[j]=w[j];
       g[j]=h[j];
       p[j]=-g[j]+beta*p[j]; // Mise à jour de la direction de descente /*@(Eq. \ref{DirDes})@*/
    }
    if(!(Epoque%5) && disp)
      printf("Epoque:%ld Loss:%lf\n",Epoque,NewLoss);
    
    Epoque++;
  }
  
  free((char *) wold);
  free((char *) p);
  free((char *) g);
  free((char *) h);
}
