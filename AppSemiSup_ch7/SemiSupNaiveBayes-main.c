/* --------------------------------------------------------------------------*
 * Algorithme naive-bayes semi-supervisé pour la classification de documents *
 * Auteur : Massih-Reza AMINI                                                *
 * Date   : 9 avril 2014                                                     *
 * --------------------------------------------------------------------------*/

#include "defs.h"

int main(int argc, char **argv)
{
  char       input_filename[100], params_filename[200];
  SPARSEVECT *Indx_Lab, *Indx_Unl;
  long int   i,j,k, m, u, d,K, *Desire, MaxTaille;
  double     **theta, *pi;
  LR_PARAM   input_params;
  FILE       *fd;
  srand(time(NULL));
  lire_commande_CEMss_naiveBayes(&input_params,input_filename, params_filename,argc, argv);

 // FileSparseVectors(input_filename,&m,&K,&d,&MaxTaille);
  FileScanSemiSup(input_filename,&m,&u,&K,&d,&MaxTaille);
  printf("La base d'apprentissage contient %ld exemples non-étiquetés, %ld exemples étiquetés répartis suivant %ld classes, en dimension %ld\n",u,m,K,d);


  Indx_Lab=(SPARSEVECT *)Allocation((m+1)*sizeof(SPARSEVECT ));
  for(i=1; i<=m; i++)
     Indx_Lab[i].Att=(FEATURE *)Allocation((MaxTaille+1)*sizeof(FEATURE ));

  Indx_Unl=(SPARSEVECT *)Allocation((u+1)*sizeof(SPARSEVECT ));
  for(i=1; i<=u; i++)
     Indx_Unl[i].Att=(FEATURE *)Allocation((MaxTaille+1)*sizeof(FEATURE ));
  Desire=(long int *)malloc((m+u+1)*sizeof(long int ));

  ChrgMatrixSemiSup(input_filename, Indx_Lab, Indx_Unl, Desire, u, m); 
 
  pi    =(double *)malloc((K+1)*sizeof(double ));
  theta = (double **) malloc((d+1)*sizeof(double *));
  if(!theta){
    printf("Probleme d'allocation de la matrice des données\n");
    exit(0);
  }
  theta[1]=(double *)malloc((size_t)((K*d+1)*sizeof(double)));
  if(!theta[1]){
    printf("Probleme d'allocation de la matrice des données\n");
    exit(0);
  }
  for(j=2; j<=d; j++)
	  theta[j]=theta[j-1]+K;
	  
	  /*
  D=(long int *)malloc((m+u+1)*sizeof(long int ));
  for(i=1; i<=m+u; i++)
	  D[i]=0;
  fd1=fopen("Training","w");
  fd2=fopen("Test","w");
  M=m*0.75;
  i=1;

  printf("%ld ",M);
  while(i<=100){
    x=(rand()%(m+u))+1;
    if(D[x]==0){
	  i++;
      D[x]=1;
	  fprintf(fd1,"%ld ",Desire[x]);
      for(j=1; j<=Indx_Lab[x].NbElt; j++)
            fprintf(fd1,"%ld:%lf ",Indx_Lab[x].Attribut[j].indice,Indx_Lab[x].Attribut[j].valeur);
       fprintf(fd1,"\n");
     }
   
  }

  while(i<=M){
    x=(rand()%(m+u))+1;
    if(D[x]==0){
	  i++;
      D[x]=1;
	  fprintf(fd1,"0 ");
      for(j=1; j<=Indx_Lab[x].NbElt; j++)
            fprintf(fd1,"%ld:%lf ",Indx_Lab[x].Attribut[j].indice,Indx_Lab[x].Attribut[j].valeur);
       fprintf(fd1,"\n");
     }
  }
  fclose(fd1);
  for(i=1; i<=m; i++)
  {
    if(D[i]==0)
	{
      fprintf(fd2,"%ld ",Desire[i]);
      for(j=1; j<=Indx_Lab[i].NbElt; j++)
            fprintf(fd1,"%ld:%lf ",Indx_Lab[i].Attribut[j].indice,Indx_Lab[i].Attribut[j].valeur);
       fprintf(fd2,"\n");

	}
  }
  fclose(fd2);
  
  exit(0); */
  CEMss_NaiveBayes(Indx_Lab, Desire, Indx_Unl, m, u, d, K, input_params.lambda, input_params.eps, input_params.T, input_params.display, theta, pi);

  if(!(fd=fopen(params_filename,"w")))
  {
    printf("Erreur de creation du fichier %s\n",params_filename);
	exit(0);
  }
  for(k=1; k<=K; k++)
	  fprintf(fd,"%lf ",pi[k]);
//  fprintf(fd,"\n");
  for(j=1; j<=d; j++){
     for(k=1; k<=K; k++)
        fprintf(fd,"%lf ",theta[j][k]);
  //    fprintf(fd,"\n");
  }

  fclose(fd);

}


void lire_commande_CEMss_naiveBayes(LR_PARAM *ss_input_params, char *fic_apprentissage, char *fic_params, int num_args, char **args)
{
  long int i;

  ss_input_params->eps=1e-4;
  ss_input_params->T=10000;
  ss_input_params->display=1;
  ss_input_params->lambda=1.0;

  for(i=1; (i<num_args) && (args[i][0] == '-'); i++){
    switch((args[i])[1]){
      case 'e': i++; sscanf(args[i],"%lf",&ss_input_params->eps); break;
      case 't': i++; sscanf(args[i],"%ld",&ss_input_params->T); break;
      case 'l': i++; sscanf(args[i],"%lf",&ss_input_params->lambda); break;      
      case 'd': i++; sscanf(args[i],"%d",&ss_input_params->display); break;
      case '?': i++; aide();exit(0); break;
      
      default : printf("Option inconnue %s\n",args[i]);Standby();aide();exit(0);
    }
  }
  if((i+1)>=num_args){
    printf("\n ---------------------------- \n Nombre de parametres d'entree insuffisant \n ----------------------------\n\n");
    Standby();
    aide();
    exit(0);
  }
  //printf("%s %s\n",args[i],args[i+1]);
  strcpy(fic_apprentissage, args[i]);
  strcpy(fic_params, args[i+1]);
}

void Standby(){
  printf("\nAide ... \n");
  (void)getc(stdin);
}

void aide(){
  printf("\nL'algorithme naive-Bayes semi-supervisé \n");
  printf("\nAuteur: Massih R. Amini \n");
  printf("Date: 8 avril 2014\n\n");
  printf("usage: CEMssNB [options] fichier_apprentissage fichier_parametres\n\n");
  printf("Options:\n");
  printf("      -e             -> précision (défaut, 1e-4)\n");
  printf("      -t             -> Nombre maximum d'itérations (défaut, 10000)\n");
  printf("      -l             -> Facteur d'implication des données non-étiquetées dans l'apprentissage (défaut, 1)\n");
  printf("      -d             -> Affichage (défaut, 0)\n");
  printf("      -?             -> cette aide\n");
  printf("Arguments:\n");
  printf("     fichier_apprentissage -> fichier contenant les exemples non-étiquetés\n");
  printf("     fichier_parametres    -> fichier contenant paramètres estimés du modèle\n\n"); 
}

