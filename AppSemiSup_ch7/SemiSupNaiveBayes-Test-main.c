/* --------------------------------------------------------------------------*
 * Algorithme naive-bayes semi-supervisé pour la classification de documents *
 * Auteur : Massih-Reza AMINI                                                *
 * Date   : 9 avril 2014                                                     *
 * --------------------------------------------------------------------------*/

#include "defs.h"

int main(int argc, char **argv)
{
  char       input_filename[100], params_filename[200];
  SPARSEVECT *Indx_Tst;
  long int   n, i,j,d,k, K, *Desire,MaxTaille;
  double     **theta, *pi;
  FILE  *fd;
  
  lire_commande_naiveBayes_Tst(input_filename, params_filename,argc, argv);

  FileSparseVectors(input_filename,&n,&K,&d,&MaxTaille);
  printf("La base d'apprentissage contient %ld exemples suivant %ld classes, en dimension %ld\n",n,K,d);


  Indx_Tst=(SPARSEVECT *)Allocation((n+1)*sizeof(SPARSEVECT ));
  for(i=1; i<=n; i++)
     Indx_Tst[i].Att=(FEATURE *)Allocation((MaxTaille+1)*sizeof(FEATURE ));

  Desire=(long int *)malloc((n+1)*sizeof(long int ));

  ChrgMatrixSparseVectors(input_filename, Indx_Tst, Desire, n); 
 
  pi    =(double *)malloc((K+1)*sizeof(double ));
  theta = (double **) malloc((d+1)*sizeof(double *));
  if(!theta){
    printf("Probleme d'allocation de la matrice des données\n");
    exit(0);
  }
  theta[1]=(double *)malloc((size_t)((K*d+1)*sizeof(double)));
  if(!theta[1]){
    printf("Probleme d'allocation de la matrice des données\n");
    exit(0);
  }
  for(j=2; j<=d; j++)
	  theta[j]=theta[j-1]+K;


  if(!(fd=fopen(params_filename,"r")))
  {
    printf("Erreur de creation du fichier %s\n",params_filename);
	exit(0);
  }
  for(k=1; k<=K; k++)
	  fscanf(fd,"%lf ",&pi[k]);
  for(j=1; j<=d; j++)
     for(k=1; k<=K; k++)
        fscanf(fd,"%lf ",&theta[j][k]);

  fclose(fd);



  NaiveBayesTest(Indx_Tst, Desire, n, d, K, theta, pi);
}


void lire_commande_naiveBayes_Tst(char *fic_test, char *fic_params, int num_args, char **args)
{
  long int i=1;

  if((i+1)>=num_args){
    printf("\n ---------------------------- \n Nombre de parametres d'entree insuffisant \n ----------------------------\n\n");
    Standby();
    aide();
    exit(0);
  }
  strcpy(fic_test, args[i]);
  strcpy(fic_params, args[i+1]);
}

void Standby(){
  printf("\nAide ... \n");
  (void)getc(stdin);
}

void aide(){
  printf("\nL'algorithme naive-Bayes semi-supervisé  test \n");
  printf("\nAuteur: Massih R. Amini \n");
  printf("Date: 8 avril 2014\n\n");
  printf("usage: NBtest fichier_test fichier_parametres\n\n");
  printf("Arguments:\n");
  printf("     fichier_test       -> fichier contenant les exemples test");
  printf("     fichier_parametres -> fichier contenant paramètres estimés du modèle\n\n"); 
}

