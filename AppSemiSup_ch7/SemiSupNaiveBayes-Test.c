
#include "defs.h"

void NaiveBayesTest(SPARSEVECT *Xt, long int   *EtiqCls, long int n, long int d, long int K, double **theta, double *pi)
{
  long int i, j, k, maxCls;
  double   logProbaJointe, max, Accuracy=0.0;

     
  for(i=1; i<=n; i++){
	logProbaJointe=log(pi[1]);
    for(j=1; j<=Xt[i].N; j++)        
        logProbaJointe+=Xt[i].Att[j].val*log(theta[Xt[i].Att[j].ind][1]);
    
    for(k=2, max=logProbaJointe, maxCls=1; k<=K; k++){
        logProbaJointe=log(pi[k]);
        for(j=1; j<=Xt[i].N; j++)        
           logProbaJointe+=Xt[i].Att[j].val*log(theta[Xt[i].Att[j].ind][k]);

        if(max<logProbaJointe){
           max=logProbaJointe;
           maxCls=k;
        }

	  //  printf("max=%lf logProbaJointe=%lf\n",max,logProbaJointe);
		
     }

     if(EtiqCls[i]==maxCls)
		 Accuracy+=1.0;
  }

  printf("Accuracy=%lf\n",Accuracy/(double ) n);
}

